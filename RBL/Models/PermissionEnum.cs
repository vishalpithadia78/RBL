﻿using System;

namespace RBL.Enums
{
    public enum Permission
    {
        CategoryView = 2,
        CategoryEdit = 3,
        CategoryAdd = 4,
        FileView = 5,
        FileEdit = 6,
        FileAdd = 7,
        UserView = 8,
        UserEdit = 9,
        UserAdd = 10
    }

}