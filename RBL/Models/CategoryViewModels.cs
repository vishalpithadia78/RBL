﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace RBL.Models
{
    public class ParentCategoryViewModel
    {
        [Display(Name = "Sr No.")]
        public Int64 Id { get; set; }

        [Display(Name = "Category Name")]
        public string Name { get; set; }

        public IEnumerable<long> ids { get; set; }

        [Display(Name = "Date")]
        public DateTime? Created_At { get; set; }

        [Display(Name = "is Active")]
        public bool? isActive { get; set; }
    }
    

    public class ChildCategoryViewModel
    {
        [Key]
        [Display(Name = "Sr No.")]
        public long? Id { get; set; }

        [Display(Name = "Sub Category")]
        public string SubName { get; set; }

        [Display(Name = "Image")]
        public string Image { get; set; }

        IEnumerable<long> ids { get; set; }

        [Display(Name = "Main Category")]
        public string ParentName { get; set; }

        [Display(Name = "Discription")]
        public string Discription { get; set; }

        [Display(Name = "Date")]
        public DateTime? Date { get; set; }

        [Display(Name = "is Active")]
        public bool? isActive { get; set; }

        public long parent_id { get; set; }
    }

    public class ChildCategoryAddViewModel
    {
        [Required]
        [Display(Name = "Main Category")]
        public long? ParentCategory { get; set; }

        public List<SelectListItem> ParentCategoryList { get; set; }
        
        public IEnumerable<long> ActiveDeactiveIds { get; set; }

        [Required]
        [Display(Name = "Sub Category")]
        [StringLength(60, ErrorMessage = "Please enter maximum 60 charachters")]
        public string SubCategory { get; set; }

        [Required]
        [StringLength(150, ErrorMessage = "Please enter maximum 150 charachters")]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Image")]
        //[FileExtensions(Extensions = ".jpg,.jpeg,.png")]
        public HttpPostedFileBase image { get; set; }
    }


    public class ChildCategoryEditViewModel
    {
        [Required]
        [Display(Name = "Main Category")]
        public long ParentCategory { get; set; }

        public List<SelectListItem> ParentCategoryList { get; set; }

        [Required]
        [Display(Name = "Sub Category")]
        [StringLength(60, ErrorMessage = "Please enter maximum 60 charachters")]
        public string SubCategory { get; set; }

        [Required]
        [StringLength(150, ErrorMessage = "Please enter maximum 150 charachters")]
        public string Description { get; set; }

        //[FileExtensions(Extensions = ".jpg,.jpeg,.png")]
        public HttpPostedFileBase image { get; set; }

        [Display(Name = "Image")]
        public string selectedImage { get; set; }

        public long? Id { get; set; }
    }

    public class ParentCategoryAddViewModel
    {


        [Required(ErrorMessage ="Please Enter Name of the Category")]
        [Display(Name = "Category Name")]
        [StringLength(60,ErrorMessage ="Please enter maximum 60 charachters")]
        public string Name { get; set; }
    }

    public class ParentCategoryEditViewModel 
    {
        [Key]
        [Display(Name = "id")]
        public long Id { get; set; }

        [Required]
        [Display(Name = "Category Name")]
        [StringLength(60, ErrorMessage = "Please enter maximum 60 charachters")]
        public string Name { get; set; }
    }
}