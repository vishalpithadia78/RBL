﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using System.Data.SqlClient;
using System.Diagnostics;

namespace RBL.Models
{
    public class HomeViewModel
    {
        public List<SelectListItem> PrentCategory { get; set; }
        public List<SubCategoryView> SubCategory { get; set; }
        public long? ParentId { get; set; }
        public long? Id { get; set; }
        public string SearchText { get; set; }
    }

    public class Dashboard
    {
        public long? Total { get; set; }
        public long? Image { get; set; }
        public long? pdf { get; set; }
        public long? video { get; set;}
    }

    public class DetailViewModel
    {
        // for search listing 
        public IEnumerable<SelectListItem> CategoryList { get; set; }
        public IEnumerable<SubCategory> SubCategoryList { get; set; }
        public long? ParentId { get; set; }
        public long? Id { get; set; }
        public string SearchText {get;set;}
        //for detail view
        public List<RBL_MediaMaster> allFile { get; set; }
        public List<int> allFileTypes { get; set; }
        public List<string> Keywords { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime? Date { get; set; }
        public List<string> Language { get; set; }
        public List<string> Location { get; set; }
        public string Author { get; set; }
        public string AuthotEmail { get; set; }
        public string Mobile { get; set; }

    }

    public class SearchViewModel
    {

        public List<string> FileTypes { get; set; }
        public List<string> Year { get; set; }
        public List<string> Locations { get; set; }
        public List<string> Language { get; set; }
        public List<SelectListItem> PdfTypeList { get; set; }
        public IEnumerable<string> SelectedYear { get; set; }
        public IEnumerable<string> SelectedLocation { get; set; }
        public IEnumerable<string> SelectedLanguage { get; set; }
        public IEnumerable<long?> SelectedFileTypes { get; set; }
        public IEnumerable<SelectListItem> CategoryList { get; set; }
        public IEnumerable<dynamic> SubCategoryList { get; set; }
        public List<SelectListItem> FileTypeList { get; set; }
        public long? pdfType { get; set; }
        public long? ParentId { get; set; }
        public int? Page  { get; set; }
        public int? PageSize { get; set; }
        public long? Id { get; set; }
        public string SearchText { get; set; }
    }

    public class SubCategoryView
    {
        public long? Id { get; set; }
        public long? ParentId { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
    }

    public class ListingModelHelper
    {
        public static IEnumerable<int> getAllYears()
        {
            using (var db = new mobisnuu_rblEntities())
            {
                return db.RBL_MediaMaster.Select(x => x.created_at.Value.Year).Distinct().ToList();
            }
        }

        public static IEnumerable<string> getAllLanguage()
        {
            using (var db = new mobisnuu_rblEntities())
            {
                return db.RBL_Language.Select(x => x.name).Distinct().ToList();
            }
        }

        public static IEnumerable<string> getAllLocation()
        {
            using (var db = new mobisnuu_rblEntities())
            {
                return db.RBL_Location.Select(x => x.name).Distinct().ToList();
            }
        }
    }

    public class SearchModel
    {
        private SearchViewModel model;

        public SearchModel(SearchViewModel model)
        {
            this.model = model;
        }

        public List<RBL_MediaMaster> getSearchResults()
        {
            List<RBL_MediaMaster> list;
            using (var db = new mobisnuu_rblEntities())
            {
                var listOfIds = SearchText();
                 list = db.RBL_MediaMaster.Where(x => listOfIds.Contains(x.id) && x.is_active == false).ToList();
            }
            return list;
        }
        public IEnumerable<long?> SearchText()
        {
            using (var db = new mobisnuu_rblEntities())
            {
                List<SqlParameter> paramter = new List<SqlParameter>(); // for sql injection prevention 
                var sqlQuery = "SELECT  distinct [id]  "
                                + "FROM [dbo].[view_complete_master] "
                                + "where  [dbo].[view_complete_master].is_active = 0 AND"
                                + "( view_complete_master.title  like '%' + @SearchText +'%'"//" + model.SearchText + "
                                + " OR view_complete_master.description  like '%' + @SearchText + '%'"//" + model.SearchText + "
                                + " OR view_complete_master.keword  like '%' + @SearchText + '%' )"; //" + model.SearchText + "
                paramter.Add(new SqlParameter("SearchText", model.SearchText ?? ""));

                if (model.ParentId != null)
                {
                    sqlQuery += " and view_complete_master.parent_cat_id = @ParentId";// + model.ParentId
                    paramter.Add(new SqlParameter("ParentId", model.ParentId));
                    if (model.Id != null)
                    {
                        sqlQuery += " and view_complete_master.child_cat_id  = @Id"; // + model.Id
                        paramter.Add(new SqlParameter("Id", model.Id));
                    }
                }

                if (model.SelectedFileTypes != null)
                {
                    if (!model.SelectedFileTypes.Contains(0))
                    {
                        sqlQuery += " AND ( ";
                        var length = model.SelectedFileTypes.Count();
                        for (int i = 0; i < length; i++)
                        {
                            if (i == 0)
                            {
                                sqlQuery += " ( [view_complete_master].[filetype_parent_id] =  @FileParentId_" + i + " "; //" + model.SelectedFileTypes.ElementAt(i) + "
                                if (model.SelectedFileTypes.ElementAt(i) == 3 && model.pdfType != null)
                                {
                                    sqlQuery += "and [view_complete_master].[filetype_child_id] = @FileChildId_" + i + " ";  //" + model.pdfType + " ";
                                    paramter.Add(new SqlParameter("FileChildId_" + i, model.pdfType));
                                }
                                sqlQuery += " ) ";
                            }
                            else
                            {
                                sqlQuery += " OR ( [view_complete_master].[filetype_parent_id] = @FileParentId_" + i + " "; //" + model.SelectedFileTypes.ElementAt(i) + "
                                if (model.SelectedFileTypes.ElementAt(i) == 3 && model.pdfType != null)
                                {
                                    sqlQuery += "and [view_complete_master].[filetype_child_id] = @FileChildId_" + i + " ";  //" + model.pdfType + " ";
                                    paramter.Add(new SqlParameter("FileChildId_" + i, model.pdfType));
                                }
                                sqlQuery += " ) ";
                            }
                            paramter.Add(new SqlParameter("FileParentId_" + i, model.SelectedFileTypes.ElementAt(i)));
                        }
                        sqlQuery += " )";
                    }
                }


                if (model.SelectedYear != null)
                {
                    sqlQuery += " AND ( ";
                    var length = model.SelectedYear.Count();
                    for (int i = 0; i < length; i++)
                    {
                        if (i == 0)
                            sqlQuery += "[view_complete_master].[date] like '%' + @SelectedYear_" + i + " + '%' "; // model.SelectedYear.ElementAt(i)
                        else
                            sqlQuery += "And [view_complete_master].[date] like '%' + @SelectedYear_" + i + " + '%' "; // model.SelectedYear.ElementAt(i)
                        paramter.Add(new SqlParameter("SelectedYear_" + i, model.SelectedYear.ElementAt(i)));
                    }
                    sqlQuery += " )";
                }
                if (model.SelectedLanguage != null || model.SelectedLocation != null)
                {
                    sqlQuery += " AND ( ";
                    if (model.SelectedLanguage != null)
                    {
                        var length = model.SelectedLanguage.Count();
                        for (int i = 0; i < length; i++)
                        {
                            if (i == 0)
                                sqlQuery += " [view_complete_master].[language] like '%' + @SelectedLanguage_" + i + " + '%' "; //model.SelectedLanguage.ElementAt(i)
                            else
                                sqlQuery += " OR [view_complete_master].[language] like '%' + @SelectedLanguage_" + i + " + '%' "; //model.SelectedLanguage.ElementAt(i)
                            paramter.Add(new SqlParameter("SelectedLanguage_" + i, model.SelectedLanguage.ElementAt(i)));
                        }
                    }

                    if (model.SelectedLocation != null)
                    {
                        var length = model.SelectedLocation.Count();
                        for (int i = 0; i < length; i++)
                        {
                            if (model.SelectedLanguage == null && i == 0)
                                sqlQuery += "[view_complete_master].[location] like '%' + @SelectedLocation_" + i + " + '%' "; //model.SelectedLocation.ElementAt(i)
                            else
                                sqlQuery += "OR [view_complete_master].[location] like '%' + @SelectedLocation_" + i + " + '%' "; //model.SelectedLocation.ElementAt(i)
                            paramter.Add(new SqlParameter("SelectedLocation_" + i, model.SelectedLocation.ElementAt(i)));
                        }

                    }

                    sqlQuery += " )";
                }
                var arrays = paramter.ToArray();
                var list = db.Database.SqlQuery<long?>(sqlQuery,arrays).ToList();

                Debug.Print(db.Database.SqlQuery<long?>(sqlQuery, arrays).ToString());

                var onlyParent = new List<long?> ();
                var tempList = db.RBL_MediaMaster.Where(x => list.Contains(x.id)).ToList();
                foreach (var item in tempList)
                {
                    onlyParent.Add(item.parent_id == 0 ? item.id : item.parent_id);
                }
                onlyParent = onlyParent.Distinct().ToList();
                return onlyParent;
            }
        }
    }
}