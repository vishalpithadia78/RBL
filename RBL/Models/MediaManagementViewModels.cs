﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Web.Mvc;
using System.Web;
using RBL.Helpers;
namespace RBL.Models
{
    public class BaseModel {

    }

    public class SubCategory
    {
        public long? Id { get; set; }
        public long Value { get; set; }
        public string Text { get; set; }
    }

    public class KeyValuePair
    {
        public string Id { get; set; }
        public string Type { get; set; }
    }

    public class InsertMediaCommonViewModel : BaseModel
    {   
        /// Category and SubCategory section
        [Required]
        [Display(Name = "Category")]
        public string Category { get; set; }
        public IEnumerable<SelectListItem> CategoryList { get; set; } = new List<SelectListItem> { new SelectListItem { Value = "1", Text = "first" }, new SelectListItem { Value = "2", Text = "second" } };

        [Key]
        public long Id { get; set; }

        /// SubCategory
        [Required]
        [Display(Name = "Sub Category")]
        public string SubCategory { get; set; }
        public IEnumerable<dynamic> SubCategoryList { get; set; }
        
        [Display(Name = "Pdf Type")]
        public IEnumerable<long> pdfType { get; set; }
        public IEnumerable<SelectListItem> PdfTypeList { get; set; }

        /// To check if it is a project or not
        [Display(Name = "Project")]
        public string Project { get; set; }
        public IEnumerable<KeyValuePair> ProjectOption { get; set; }



        public bool IsProject() {
            if (Project == "1")
                return true;
            else if (Project == "0")
                return false;
            else
            {
                System.Diagnostics.Debug.WriteLine("default ");
                return false;
            }
        }



        /// Title and Description
        [Required]
        [Display(Name = "Title")]
        [StringLength(60, ErrorMessage = "Please enter maximum 60 charachters")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Description")]
        [StringLength(150, ErrorMessage = "Please enter maximum 150 charachters")]
        public string Description { get; set; }
        
        /// Keywords, Location, Language
        [Required]
        [Display(Name = "Keywords")]
        public string Keywords { get; set; }
        
        [Required]
        [Display(Name = "Location")]
        public string Location { get; set; }
        
        [Required]
        [Display(Name = "Language")]
        public string Language { get; set; }

        /// some utility fuctions
        public string[]  getKeywords() {
           return Keywords.Split(',');
        }

        public string[] getLocation()
        {
            return Location.Split(',');
        }

        public string[] getLanguage()
        {
            return Language.Split(',');
        }

        /// file types and files
        [Display(Name = "Select Type")]
        public string SelectTypeNonProject { get; set; }
        
        [Display(Name = "Select Type")]
        public IEnumerable<Int32> SelectTypeProject { get; set; }

        public IEnumerable<KeyValuePair> SelectTypeNonProjectOption { get; set; }

        /// when project
        
        [Display(Name = "Video Path")]
        public IEnumerable<string> videolist { get; set; }

       // [FileExtensions(Extensions = (".jpg,.jpeg,.png"), ErrorMessage = "Please select an image file.")]
        [Display(Name = "Video Thumb")]
        [ListFileExtension(".jpg,.jpeg,.png",ErrorMessage ="Please Provide Image files")]
        public IEnumerable<HttpPostedFileBase> videoThumblist { get; set; }
        
        //[FileExtensions(Extensions = (".jpg,.jpeg,.png"), ErrorMessage = "Please select an image file.")]
        [Display(Name = "Image")]
        [ListFileExtension(".jpg,.jpeg,.png",ErrorMessage ="Please Provide Image files")]
        public IEnumerable<HttpPostedFileBase> imagelist { get; set; }

        //[FileExtensions(Extensions = (".pdf"), ErrorMessage = "Please select an pdf file.")]
        [Display(Name = "Pdf")]
        [ListFileExtension(".pdf",ErrorMessage ="Please Provide Pdf files")]
        public IEnumerable<HttpPostedFileBase> pdflist { get; set; }

    }

    public class IndexMediaViewModel
    {
        public long? Id { get; set; }

        [Display(Name = "Image")]
        public string Image { get; set; }

        [Display(Name = "Category")]
        public string Category { get; set; }

        [Display(Name = "Type")]
        public string Type { get; set; }

        [Display(Name = "Title")]
        public string Title { get; set; }

        [Display(Name = "Date")]
        public DateTime? Date { get; set; }

        public bool isActive { get; set; }

    }

    public class UploadedContent
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long? Type { get; set; }
        public string thumb { get; set; }
    }

    public class EditMediaViewModel : InsertMediaCommonViewModel
    {
        [Display(Name = "Video Path")]
        public IEnumerable<string> UploadedVideolist { get; set; }
        public IEnumerable<long> UploadedVideo { get; set; }
        public IEnumerable<string> UploadVideoText { get; set; }
        public IEnumerable<UploadedContent> UploadedVideoThumblist { get; set; }

        public string CategoryName { get; set; }
        public string SubCategoryName { get; set; }
        public IEnumerable<long> UploadedImage { get; set; }
        public IEnumerable<UploadedContent> UploadedImagelist { get; set; }
        
        [Display(Name = "Pdf Type")]
        public IEnumerable<long> uploadedPdfType { get; set; }
        public IEnumerable<long> UploadedPdf { get; set; }
        public IEnumerable<UploadedContent> UploadedPdflist { get; set; }
    }
}