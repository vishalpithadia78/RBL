﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Web.Mvc;
using System.Web;

namespace RBL.Models
{
    // roles section
    public class RolesComman {

        [Required]
        [Display(Name = "Name")]
        [StringLength(60, ErrorMessage = "Please enter maximum 60 charachters")]
        public string RoleName { get; set; }

        [Key]
        public long Id { get; set; }

        [Display(Name = "Category :")]
        public IEnumerable<int> CategoryManagement { get; set; }

        [Display(Name = "File")]
        public IEnumerable<int> FileManagement { get; set; }

        public IEnumerable<KeyValuePair> CategoryPermission { get; set; }

        public IEnumerable<KeyValuePair> FilePermission { get; set; }

        public IEnumerable<KeyValuePair> UserPermission { get; set; }

    }


    public class RolesList
    {
        [Key]
        [Display(Name = "Sr. no")]
        public long Id { get; set; }

        [Display(Name = "Role Name")]
        public string RoleName { get; set; }

        [Display(Name = "Date")]
        public DateTime? Date { get; set; }

        public bool? isActive { get; set; }

    }

    // user Sections

    public class UserComman
    {
        public long? Id { get; set; }

        [Required]
        [Display(Name = "Name")]
        [RegularExpression(@"^[a-z0-9_-]{3,15}$", ErrorMessage = "Please enter a valid name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Email ID")]
        [DataType(DataType.EmailAddress)]
        [StringLength(40, ErrorMessage = "Please enter maximum 40 charachters")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Please enter a valid e-mail adress")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        [StringLength(20, ErrorMessage = "Please enter minimus 8 and maximum 20 charachters",MinimumLength = 8)]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@!%*?&+=#]).{8,40}$", ErrorMessage = "minimum of 1 lower case letter ,1 upper case letter,1 numeric character,1 special character:")]
        public string password { get; set; }

        [Required]
        [Display(Name = "Employee Code")]
        [StringLength(20, ErrorMessage = "Please enter maximum 20 charachters")]
        public string EmpCode { get; set; }

        [Required]
        [Display(Name = "Mobile No.")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^((\+)?(\d{2})?(\d{2}[-])?)?(\d{10}){1}?$", ErrorMessage = "Minimum of 1 lower case letter ,1 upper case letter,1 numeric character,1 special character:")]
        public string Mobile { get; set; }

        [Required]
        [Display(Name = "Role")]
        public long? Role { get; set; }

        public IEnumerable<KeyValuePair> CategoryPermission { get; set; }

        public IEnumerable<KeyValuePair> FilePermission { get; set; }

        public IEnumerable<KeyValuePair> UserPermission { get; set; }

        public List<ListItemRoles> RoleList { get; set; }
    }

    public class ListItemRoles
    {
        public string RoleName { get; set; }

        public long Id { get; set; }

        public List<long> permissions { get; set; }

    }


    public class UserEdit 
    {
        public long? Id { get; set; }

        [Required]
        [Display(Name = "Name")]
        [RegularExpression(@"^[a-z0-9_-]{3,15}$", ErrorMessage = "Please enter a valid name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Email ID")]
        [DataType(DataType.EmailAddress)]
        [StringLength(40, ErrorMessage = "Please enter maximum 40 charachters")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Please enter a valid e-mail adress")]
        public string Email { get; set; }
        
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        [StringLength(20, ErrorMessage = "Please enter minimus 8 and maximum 20 charachters", MinimumLength = 8)]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@!%*?&+=#]).{8,40}$", ErrorMessage = "Minimum of 1 lower case letter ,1 upper case letter,1 numeric character,1 special character:")]
        public string password { get; set; }

        [Required]
        [Display(Name = "Employee Code")]
        [StringLength(20, ErrorMessage = "Please enter maximum 20 charachters")]
        public string EmpCode { get; set; }

        [Required]
        [Display(Name = "Mobile No.")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^((\+)?(\d{2})?(\d{2}[-])?)?(\d{10}){1}?$", ErrorMessage = "Enter a Valid Mobile Number")]
        public string Mobile { get; set; }

        [Required]
        [Display(Name = "Role")]
        public long? Role { get; set; }

        public IEnumerable<KeyValuePair> CategoryPermission { get; set; }

        public IEnumerable<KeyValuePair> FilePermission { get; set; }

        public IEnumerable<KeyValuePair> UserPermission { get; set; }

        public List<ListItemRoles> RoleList { get; set; }

    }

    public class UserList
    {
        [Display(Name = "Sr. no")]
        public long Id { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Employee Code")]
        public string EmpCode { get; set; }

        [Display(Name = "Mobile")]
        public string Mobile { get; set; }

        [Display(Name = "Role")]
        public string Role { get; set; }

        public bool IsActive { get; set; }
    }
}