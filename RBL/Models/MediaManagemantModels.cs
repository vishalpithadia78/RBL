﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Web.Mvc;
using System.Web;
using System.IO;
using System.Web.Hosting;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web.SessionState;
using System.Linq;

namespace RBL.Models
{
    public class MediaModel :  IHttpHandler, IReadOnlySessionState {
        private InsertMediaCommonViewModel model { get; set; }
        private EditMediaViewModel editModel { get; set; }
        private long currentUserId;
        private long parentId = 0;
        private mobisnuu_rblEntities db ;


        public static string uploadDir { get; set; } = HostingEnvironment.MapPath("~/App_Data/uploads/");

        Dictionary<string, List<dynamic>> NonProject = new Dictionary<string, List<dynamic>>();
        Dictionary<string, List<dynamic>> Project = new Dictionary<string, List<dynamic>>();

        public static string[] getAllkeywords()
        {
            using (var db = new mobisnuu_rblEntities())
            {
                var list = db.RBL_Keyword.Select(x => x.name ).ToArray();
                return list;
            }
        }

        public static string[] getAllLocation()
        {
            using (var db = new mobisnuu_rblEntities())
            {
                var list = db.RBL_Location.Select(x => x.name).ToArray();
                return list;
            }
        }

        public static string[] getAllLanguage()
        {
            using (var db = new mobisnuu_rblEntities())
            {
                var list = db.RBL_Language.Select(x => x.name).ToArray();
                return list;
            }
        }

        public MediaModel(InsertMediaCommonViewModel model,long currentUserId) {
            this.model =  model;
            this.currentUserId = currentUserId;
            this.db = new mobisnuu_rblEntities();
        }

        public MediaModel(EditMediaViewModel model, mobisnuu_rblEntities db, long currentUserId)
        {
            editModel = model;
            this.db = db;
            this.currentUserId = currentUserId;
            this.parentId = editModel.Id;
        }

        #region Adding Section

        

        #region non Project
        public bool SaveNonProject() {
            try
            {
                if (SaveNonProjectFiles())
                {
                    if (SaveNonProjectInDB())
                        return true;
                    else
                        return false;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        private bool SaveNonProjectInDB()
        {
            using (var  transaction = db.Database.BeginTransaction()) { 
                try
                {
                    foreach (KeyValuePair<string, List<dynamic>> entry in this.Project)
                    {
                        if (entry.Key == "image" )
                        {
                            foreach (var item in entry.Value)
                            {
                                RBL_MediaMaster nonProject = new RBL_MediaMaster();
                                nonProject.user_master_id = currentUserId;
                                nonProject.is_project = false;
                                nonProject.parent_id = this.parentId;
                                nonProject.title = model.Title;
                                nonProject.description = model.Description;
                                nonProject.filetype_parent_id = Convert.ToInt64(model.SelectTypeNonProject);
                                nonProject.filetype_child_id = 0;
                                nonProject.guid = Guid.NewGuid().ToString();
                                nonProject.is_approved = Helpers.Utility.isAdmin(currentUserId);
                                var currentTime = DateTime.Now;
                                nonProject.created_by = currentUserId;
                                nonProject.updated_by = currentUserId;
                                nonProject.created_at = currentTime;
                                nonProject.updated_at = currentTime;
                                nonProject.thumbnail = item.file;
                                nonProject.filename = item.file;
                                nonProject.extension = item.extension;
                                nonProject.size = item.fileSize;
                                db.RBL_MediaMaster.Add(nonProject);
                                if (parentId == 0)
                                {
                                    db.SaveChanges();
                                    this.parentId = nonProject.id;
                                }
                            }
                        }
                        else if (entry.Key == "videoThumb")
                        {
                            var videoThumbsAndVideoText = model.videolist.Zip(entry.Value, (n, w) => new { text = n, thumbObj = w });
                            foreach (var nonProjectEntry in videoThumbsAndVideoText)
                            {
                                var dbObj = getProjectEntryObject();
                                dbObj.is_project = false;
                                try
                                {
                                    dbObj.extension = Path.GetExtension(nonProjectEntry.text);
                                }
                                catch (Exception)
                                {
                                    dbObj.extension = "video'";
                                }
                                dbObj.thumbnail = nonProjectEntry.thumbObj.file;
                                dbObj.filename = nonProjectEntry.text;
                                dbObj.filetype_parent_id = 2;
                                dbObj.filetype_child_id = 0;
                                db.RBL_MediaMaster.Add(dbObj);
                                if (parentId == 0)
                                {
                                    db.SaveChanges();
                                    this.parentId = dbObj.id;
                                }
                            }

                        }
                        else if (entry.Key == "pdf")
                        {
                            var videoThumbsAndVideoText = model.pdfType.Zip(entry.Value, (n, w) => new { pdfTypeId = n, Obj = w });
                            foreach (var nonProjectEntry in videoThumbsAndVideoText)
                            {
                                var dbObj = getProjectEntryObject();
                                dbObj.is_project = false;
                                dbObj.extension = nonProjectEntry.Obj.extension;
                                dbObj.thumbnail = nonProjectEntry.Obj.thumb;
                                dbObj.filename = nonProjectEntry.Obj.file;
                                dbObj.filetype_parent_id = 3;
                                dbObj.filetype_child_id = nonProjectEntry.pdfTypeId;
                                db.RBL_MediaMaster.Add(dbObj);
                                if (parentId == 0)
                                {
                                    db.SaveChanges();
                                    this.parentId = dbObj.id;
                                }
                            }
                        }
                        else throw new Exception("No key found in dictionary");

                    }
                    db.SaveChanges();
                    if (SaveTaxonomy())
                    {
                        transaction.Commit();
                        return true;
                    }
                    else
                    {
                        throw new Exception("Taxonomieas not stored");
                    }
                }
                catch (Exception ex)
                {
                    foreach (KeyValuePair<string, List<dynamic>> entry in this.Project)
                    {
                        if (entry.Key == "image" || entry.Key == "videoThumb")
                        {
                            foreach (var item in entry.Value)
                            {
                                File.Delete(MediaModel.uploadDir + (string)item.file);
                            }
                        }
                        else if (entry.Key == "pdf")
                        {
                            foreach (var item in entry.Value)
                            {
                                File.Delete(MediaModel.uploadDir + (string)item.thumb);
                            }
                        }

                    }
                    transaction.Rollback();
                    return false;
                }
            }
        }

        private bool SaveTaxonomy( bool isEdit = false) {
            return (saveCategory(isEdit) && saveKeyword(isEdit) && saveLanguage(isEdit) && saveLocation(isEdit));
        }

        private bool saveCategory(bool isEdit = false)
        {
            try
            {
                var dblist = db.RBL_MediaMaster.Where(x => x.parent_id == parentId || x.id == parentId).Select(x => x.id).ToList();
                //RBL_MediaMasterCategory catObj = new RBL_MediaMasterCategory();
                //catObj.category_child_id = Convert.ToInt64(model.SubCategory);
                //catObj.category_id = Convert.ToInt64(model.Category);
                //catObj.guid = Guid.NewGuid().ToString();
                //catObj.created_by = currentUserId;
                //catObj.updated_by = currentUserId;
                DateTime currentTime = DateTime.Now;
                //catObj.created_at = currentTime;
                //catObj.updated_at = currentTime;
                //catObj.media_master_id = parentId;
                //db.RBL_MediaMasterCategory.Add(catObj);
                var oldDbObj = db.RBL_MediaMasterCategory.Where(x => x.media_master_id == parentId).FirstOrDefault();
                //if (oldDbObj != null)
                //{
                //    db.Entry(oldDbObj).State = System.Data.Entity.EntityState.Deleted;
                //}
                foreach (var item in dblist)
                {
                    var catid = Convert.ToInt64(isEdit ? editModel.Category : model.Category); 
                    var childCatId = Convert.ToInt64(isEdit ? editModel.SubCategory : model.SubCategory);
                    var dbObj = db.RBL_MediaMasterCategory.Where(x => x.category_id == catid && x.category_child_id == childCatId && x.media_master_id == item).FirstOrDefault();
                    if (dbObj == null)
                    {
                        RBL_MediaMasterCategory catObjIndividual = new RBL_MediaMasterCategory();
                        catObjIndividual.category_child_id = childCatId;
                        catObjIndividual.category_id = catid;
                        catObjIndividual.guid = Guid.NewGuid().ToString();
                        catObjIndividual.created_by = currentUserId;
                        catObjIndividual.updated_by = currentUserId;
                        currentTime = DateTime.Now;
                        catObjIndividual.created_at = currentTime;
                        catObjIndividual.updated_at = currentTime;
                        catObjIndividual.media_master_id = item;
                        db.RBL_MediaMasterCategory.Add(catObjIndividual);
                    }
                }
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private bool saveKeyword(bool isEdit = false)
        {
            try
            {
                var keywords = isEdit ? editModel.getKeywords() :model.getKeywords();
                var length = keywords.Length;
                long keywordId;
                for (int i = 0; i < length; i++)
                {
                    keywordId = getKeywordId(keywords[i]);
                    //var dbObj = db.RBL_MediaMasterKeyword.Where(x => x.keyword_id == keywordId).Where(x => x.media_master_id == parentId).FirstOrDefault();
                    //if (dbObj == null)
                    //{
                    //    RBL_MediaMasterKeyword KeyObj = new RBL_MediaMasterKeyword();
                    //    KeyObj.keyword_id = keywordId;
                    //    KeyObj.guid = Guid.NewGuid().ToString();
                    //    KeyObj.created_by = currentUserId;
                    //    KeyObj.updated_by = currentUserId;
                    //    DateTime currentTime = DateTime.Now;
                    //    KeyObj.created_at = currentTime;
                    //    KeyObj.updated_at = currentTime;
                    //    KeyObj.media_master_id = parentId;
                    //    db.RBL_MediaMasterKeyword.Add(KeyObj);
                    //}
                    var dblist = db.RBL_MediaMaster.Where(x => x.parent_id == parentId || x.id == parentId).Select(x => x.id).ToList();
                    foreach (var item in dblist)
                    {
                       var dbObj = db.RBL_MediaMasterKeyword.Where(x => x.keyword_id == keywordId).Where(x => x.media_master_id == item).FirstOrDefault();
                        if (dbObj == null)
                        {
                            RBL_MediaMasterKeyword KeyObj = new RBL_MediaMasterKeyword();
                            KeyObj.keyword_id = keywordId;
                            KeyObj.guid = Guid.NewGuid().ToString();
                            KeyObj.created_by = currentUserId;
                            KeyObj.updated_by = currentUserId;
                            DateTime currentTime = DateTime.Now;
                            KeyObj.created_at = currentTime;
                            KeyObj.updated_at = currentTime;
                            KeyObj.media_master_id = item;
                            db.RBL_MediaMasterKeyword.Add(KeyObj);
                        }
                    }
                    
                }
                
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private long getKeywordId(string v)
        {
            try
            {
                RBL_Keyword keyObj = db.RBL_Keyword.Where(x => x.name.ToLower() == v.ToLower()).FirstOrDefault();
                if (keyObj != null)
                    return keyObj.id;
                else
                {
                    keyObj = new RBL_Keyword();
                    keyObj.name = v.ToLower();
                    keyObj.created_by = currentUserId;
                    keyObj.updated_by = currentUserId;
                    var currentTime = DateTime.Now;
                    keyObj.created_at = currentTime;
                    keyObj.guid = Guid.NewGuid().ToString();
                    keyObj.updated_at = currentTime;
                    db.RBL_Keyword.Add(keyObj);
                    db.SaveChanges();
                    return keyObj.id;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool saveLocation(bool isEdit = false) {
            try
            {
                var locations = isEdit?editModel.getLocation() : model.getLocation();
                var length = locations.Length;
                long locationId;
                for (int i = 0; i < length; i++)
                {
                    locationId = getLocationId(locations[i]);
                    //var dbObj = db.RBL_MediaMasterLocation.Where(x => x.location_id == locationId).Where(x => x.media_master_id == parentId).FirstOrDefault();
                    //if (dbObj == null)
                    //{
                    //    RBL_MediaMasterLocation locObj = new RBL_MediaMasterLocation();
                    //    locObj.location_id = locationId;
                    //    locObj.created_by = currentUserId;
                    //    locObj.updated_by = currentUserId;
                    //    locObj.guid = Guid.NewGuid().ToString();
                    //    DateTime currentTime = DateTime.Now;
                    //    locObj.created_at = currentTime;
                    //    locObj.updated_at = currentTime;
                    //    locObj.media_master_id = parentId;
                    //    db.RBL_MediaMasterLocation.Add(locObj);
                    //}
                    var dblist = db.RBL_MediaMaster.Where(x => x.parent_id == parentId || x.id == parentId).Select(x => x.id).ToList();
                    foreach (var item in dblist)
                    {
                        var  dbObj = db.RBL_MediaMasterLocation.Where(x => x.location_id == locationId).Where(x => x.media_master_id == item).FirstOrDefault();
                        if (dbObj == null)
                        {
                            RBL_MediaMasterLocation locObj = new RBL_MediaMasterLocation();
                            locObj.location_id = locationId;
                            locObj.created_by = currentUserId;
                            locObj.updated_by = currentUserId;
                            locObj.guid = Guid.NewGuid().ToString();
                            DateTime currentTime = DateTime.Now;
                            locObj.created_at = currentTime;
                            locObj.updated_at = currentTime;
                            locObj.media_master_id = item;
                            db.RBL_MediaMasterLocation.Add(locObj);
                        }
                    }
                }

                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private long getLocationId(string v)
        {
            try
            {
                RBL_Location locObj = db.RBL_Location.Where(x => x.name.ToLower() == v.ToLower()).FirstOrDefault();
                if (locObj != null)
                    return locObj.id;
                else
                {
                    locObj = new RBL_Location();
                    locObj.name = v.ToLower();
                    locObj.created_by = currentUserId;
                    locObj.updated_by = currentUserId;
                    var currentTime = DateTime.Now;
                    locObj.guid = Guid.NewGuid().ToString();
                    locObj.created_at = currentTime;
                    locObj.updated_at = currentTime;
                    db.RBL_Location.Add(locObj);
                    db.SaveChanges();
                    return locObj.id;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool saveLanguage(bool isEdit = false)
        {
            try
            {
                var languages = isEdit ? editModel.getLanguage() : model.getLanguage() ;
                var length = languages.Length;
                long languageId;
                for (int i = 0; i < length; i++)
                {
                    languageId = getLanguageId(languages[i]);
                    //var dbObj = db.RBL_MediaMasterLanguage.Where(x => x.language_id == languageId).Where(x => x.media_master_id == parentId).FirstOrDefault();
                    //if (dbObj == null)
                    //{
                    //    RBL_MediaMasterLanguage langObj = new RBL_MediaMasterLanguage();
                    //    langObj.language_id = languageId;
                    //    langObj.created_by = currentUserId;
                    //    langObj.updated_by = currentUserId;
                    //    langObj.guid = Guid.NewGuid().ToString();
                    //    DateTime currentTime = DateTime.Now;
                    //    langObj.created_at = currentTime;
                    //    langObj.updated_at = currentTime;
                    //    langObj.media_master_id = parentId;
                    //    db.RBL_MediaMasterLanguage.Add(langObj);
                    //}
                    var dblist = db.RBL_MediaMaster.Where(x => x.parent_id == parentId || x.id == parentId).Select(x => x.id).ToList();
                    foreach (var item in dblist)
                    {
                       var dbObj = db.RBL_MediaMasterLanguage.Where(x => x.language_id == languageId).Where(x => x.media_master_id == item ).FirstOrDefault();
                        if (dbObj == null)
                        {
                            RBL_MediaMasterLanguage langObj = new RBL_MediaMasterLanguage();
                            langObj.language_id = languageId;
                            langObj.created_by = currentUserId;
                            langObj.updated_by = currentUserId;
                            langObj.guid = Guid.NewGuid().ToString();
                            DateTime currentTime = DateTime.Now;
                            langObj.created_at = currentTime;
                            langObj.updated_at = currentTime;
                            langObj.media_master_id = item;
                            db.RBL_MediaMasterLanguage.Add(langObj);
                        }
                    }
                }
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private long getLanguageId(string v)
        {
            try
            {
                RBL_Language langObj = db.RBL_Language.Where(x => x.name.ToLower() == v.ToLower()).FirstOrDefault();
                if (langObj != null)
                    return langObj.id;
                else
                {
                    langObj = new RBL_Language();
                    langObj.name = v.ToLower();
                    langObj.created_by = currentUserId;
                    langObj.updated_by = currentUserId;
                    var currentTime = DateTime.Now;
                    langObj.created_at = currentTime;
                    langObj.guid = Guid.NewGuid().ToString();
                    langObj.updated_at = currentTime;
                    db.RBL_Language.Add(langObj);
                    db.SaveChanges();
                    return langObj.id;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private bool SaveNonProjectFiles()
        {
            try
            {
                IEnumerable<HttpPostedFileBase> listOfFilesToBeUploaded;
                if (model.SelectTypeNonProject == "1") //image
                {
                    listOfFilesToBeUploaded = model.imagelist;
                }
                else if (model.SelectTypeNonProject == "2") // video thumb
                {
                    listOfFilesToBeUploaded = model.videoThumblist;
                }
                else if (model.SelectTypeNonProject == "3") // pdf + pdf Thumb
                {
                    listOfFilesToBeUploaded = model.pdflist;
                }
                else
                {
                    throw new Exception("Select type is not found");
                }
                var fileType = Convert.ToInt32(model.SelectTypeNonProject);
                foreach (var item in listOfFilesToBeUploaded)
                {

                    if (fileType == 1) //image
                    {
                            var SavedName = SaveFile(item);
                            if (SavedName != null)
                            {
                                var obj = new { file = SavedName, fileSize = Convert.ToString(item.ContentLength), extension = Path.GetExtension(SavedName) };
                                if (Project.ContainsKey("image"))
                                {
                                    Project["image"].Add(obj);
                                }
                                else
                                {
                                    var list = (new List<dynamic>());
                                    list.Add(obj);
                                    Project.Add("image", list);
                                }
                            }
                            else
                                throw new Exception("could not upload file" + item.FileName);
                    }
                    else if (fileType == 2) // video thumb
                    {
                            var SavedName = SaveFile(item);
                            if (SavedName != null)
                            {
                                var obj = new { file = SavedName, fileSize = Convert.ToString(item.ContentLength), extension = Path.GetExtension(SavedName) };
                                if (Project.ContainsKey("videoThumb"))
                                {
                                    Project["videoThumb"].Add(obj);
                                }
                                else
                                {
                                    var list = (new List<dynamic>());
                                    list.Add(obj);
                                    Project.Add("videoThumb", list);
                                }
                            }
                            else
                                throw new Exception("could not upload file" + item.FileName);
                    }
                    else if (fileType == 3) // pdf + pdf Thumb
                    {
                            var SavedName = SaveFile(item);
                            var PdfPath = Path.Combine(MediaModel.uploadDir, SavedName);
                            var pdfthumb = PDFToImage(PdfPath, MediaModel.uploadDir, 200);
                            if (SavedName != null)
                            {
                                var obj = new { file = SavedName, fileSize = Convert.ToString(item.ContentLength), thumb = pdfthumb, extension = Path.GetExtension(SavedName) };
                                if (Project.ContainsKey("pdf"))
                                {

                                    Project["pdf"].Add(obj);
                                }
                                else
                                {
                                    var list = (new List<dynamic>());
                                    list.Add(obj);
                                    Project.Add("pdf", list);
                                }
                            }
                            else
                                throw new Exception("could not upload file" + item.FileName);

                    }
                    else
                    {
                        throw new Exception("could not found ");
                    }
                }
                return true;              

            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion

        #region project
        public bool SaveProject() {
            try
            {
                if (SaveProjectFiles())
                {
                    if (SaveProjectInDB())
                    {
                        return true;
                    }
                    else
                    {
                        throw new Exception("couldn not insert into database");
                    }
                }
                else
                {
                    throw new Exception("couldnt save files");
                }

            }
            catch (Exception ex)
            {
                if (Project.ContainsKey("error"))
                {
                    Project["error"].Add(ex.Message);
                }
                else
                {
                    var list = (new List<dynamic>());
                    list.Add(ex.Message);
                    Project.Add("error", list);
                }
                return false;
            }
        }

        private bool SaveProjectFiles()
        {
            try
            {
                foreach (var fileType in model.SelectTypeProject)
                {
                    if (fileType == 1) //image
                    {
                        foreach (var image in model.imagelist)
                        {
                            var SavedName = SaveFile(image);
                            if (SavedName != null)
                            {
                                var obj = new { file = SavedName, fileSize = Convert.ToString(image.ContentLength), extension = Path.GetExtension(SavedName) };
                                if (Project.ContainsKey("image"))
                                {
                                    Project["image"].Add(obj);
                                }
                                else
                                {
                                    var list = (new List<dynamic>());
                                    list.Add(obj);
                                    Project.Add("image", list);
                                }
                            }
                            else
                                throw new Exception("could not upload file"+image.FileName);
                        }
                    }
                    else if (fileType == 2) // video thumb
                    {
                        foreach (var videoThumb in model.videoThumblist)
                        {
                            var SavedName = SaveFile(videoThumb);
                            if (SavedName != null)
                            {
                                var obj = new { file = SavedName, fileSize = Convert.ToString(videoThumb.ContentLength), extension = Path.GetExtension(SavedName) };
                                if (Project.ContainsKey("videoThumb"))
                                {
                                    Project["videoThumb"].Add(obj);
                                }
                                else
                                {
                                    var list = (new List<dynamic>());
                                    list.Add(obj);
                                    Project.Add("videoThumb", list);
                                }
                            }
                            else
                                throw new Exception("could not upload file" + videoThumb.FileName);

                        }
                    }
                    else if (fileType == 3) // pdf + pdf Thumb
                    {
                        foreach (var pdf in model.pdflist)
                        {
                            var SavedName = SaveFile(pdf);
                            var PdfPath = Path.Combine(MediaModel.uploadDir, SavedName);
                            var pdfthumb = PDFToImage(PdfPath, MediaModel.uploadDir, 200);
                            if (SavedName != null)
                            {
                                var obj = new { file = SavedName, fileSize = Convert.ToString(pdf.ContentLength), thumb = pdfthumb, extension = Path.GetExtension(SavedName) };
                                if (Project.ContainsKey("pdf"))
                                {

                                    Project["pdf"].Add(obj);
                                }
                                else
                                {
                                    var list = (new List<dynamic>());
                                    list.Add(obj);
                                    Project.Add("pdf", list);
                                }
                            }
                            else
                                throw new Exception("could not upload file" + pdf.FileName);

                        }
                        
                    }
                    else
                    {
                    }

                }
                return true;
            }
            catch (Exception ex)
            {
                if (Project.ContainsKey("error"))
                {
                    Project["error"].Add(ex.Message);
                }
                else
                {
                    var list = (new List<dynamic>());
                    list.Add(ex.Message);
                    Project.Add("error", list);
                }
                return false;
            }
            
        }

        private bool SaveProjectInDB()
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    RBL_MediaMaster Project = new RBL_MediaMaster();
                    Project.user_master_id = currentUserId;
                    Project.is_project = true;
                    Project.parent_id = 0;
                    Project.title = model.Title;
                    Project.description = model.Description;
                    Project.filetype_parent_id = 0;
                    Project.filetype_child_id = 0;
                    Project.guid = Guid.NewGuid().ToString();
                    Project.is_approved = Helpers.Utility.isAdmin(currentUserId);
                    var currentTime = DateTime.Now;
                    Project.created_by = currentUserId;
                    Project.updated_by = currentUserId;
                    Project.created_at = currentTime;
                    Project.updated_at = currentTime;
                    // setting thumbnail for project
                    if (this.Project.ContainsKey("image"))
                    {
                        Project.thumbnail = this.Project["image"][0].file;
                    }
                    else if (this.Project.ContainsKey("pdf"))
                    {
                        Project.thumbnail = this.Project["pdf"][0].thumb;
                    }
                    else if (this.Project.ContainsKey("videoThumb"))
                    {
                        Project.thumbnail = this.Project["videoThumb"][0].file;
                    }
                    else
                    {
                        System.Diagnostics.Debug.WriteLine("couldnt find thumbnail");
                    }
                    db.RBL_MediaMaster.Add(Project);
                    db.SaveChanges();
                    this.parentId = Project.id;
                    if (saveProjectChildFilesInDB())
                    {
                        if (SaveTaxonomy())
                        {
                            transaction.Commit();
                            return true;
                        }
                        else
                        {
                            throw new Exception("Taxonomieas not stored");
                        }
                    }
                    else
                    {
                        throw new Exception("couldnot save project files in Db");
                    }
                }
                catch (Exception ex)
                {
                    foreach (KeyValuePair<string, List<dynamic>> entry in this.Project)
                    {
                        if (entry.Key == "image" || entry.Key == "videoThumb")
                        {
                            foreach (var item in entry.Value)
                            {
                                File.Delete(MediaModel.uploadDir + (string)item.file);
                            }
                        }
                        else if(entry.Key == "pdf")
                        {
                            foreach (var item in entry.Value)
                            {
                                File.Delete(MediaModel.uploadDir + (string)item.thumb);
                            }
                        }
                        
                    }
                    transaction.Rollback();
                    return false;
                }
            }
        }

        private RBL_MediaMaster getProjectEntryObject()
        {
            RBL_MediaMaster ProjectEntry = new RBL_MediaMaster();
            ProjectEntry.user_master_id = currentUserId;
            ProjectEntry.is_project = true;
            ProjectEntry.parent_id = parentId;
            ProjectEntry.title = model.Title;
            ProjectEntry.description = model.Description;
            ProjectEntry.filetype_parent_id = 0;
            ProjectEntry.filetype_child_id = 0;
            ProjectEntry.guid = Guid.NewGuid().ToString();
            ProjectEntry.is_approved = Helpers.Utility.isAdmin(currentUserId);
            var currentTime = DateTime.Now;
            ProjectEntry.created_by = currentUserId;
            ProjectEntry.updated_by = currentUserId;
            ProjectEntry.created_at = currentTime;
            ProjectEntry.updated_at = currentTime;
            return ProjectEntry;
        }

        private bool saveProjectChildFilesInDB()
        {
            try
            {
                foreach (KeyValuePair<string, List<dynamic>> entry in this.Project)
                {
                    if (entry.Key == "image")
                    {
                        foreach (var projectEntry in entry.Value)
                        {
                            var dbObj = getProjectEntryObject();
                            dbObj.extension = projectEntry.extension;
                            dbObj.filename = projectEntry.file;
                            dbObj.thumbnail = projectEntry.file;
                            dbObj.size = projectEntry.fileSize;
                            dbObj.filetype_parent_id = 1;
                            dbObj.filetype_child_id = 0;
                            db.RBL_MediaMaster.Add(dbObj);
                        }
                    }
                    else if (entry.Key == "pdf") {
                        var videoThumbsAndVideoText = model.pdfType.Zip(entry.Value, (n, w) => new { pdfTypeId = n, Obj = w });
                        foreach (var nonProjectEntry in videoThumbsAndVideoText)
                        {
                            var dbObj = getProjectEntryObject();
                            dbObj.extension = nonProjectEntry.Obj.extension;
                            dbObj.filename = nonProjectEntry.Obj.file;
                            dbObj.size = nonProjectEntry.Obj.fileSize;
                            dbObj.filetype_parent_id = 3;
                            dbObj.filetype_child_id = nonProjectEntry.pdfTypeId;
                            dbObj.thumbnail = nonProjectEntry.Obj.thumb;
                            db.RBL_MediaMaster.Add(dbObj);
                        }
                    }
                    else if (entry.Key == "videoThumb")
                    {
                        var videoThumbsAndVideoText = model.videolist.Zip(entry.Value, (n, w) => new { text = n, thumbObj = w });
                        foreach (var projectEntry in videoThumbsAndVideoText)
                        {
                            var dbObj = getProjectEntryObject();
                            try
                            {
                                dbObj.extension = Path.GetExtension(projectEntry.text);
                            }
                            catch (Exception)
                            {
                                dbObj.extension = "video'";
                            }
                            dbObj.thumbnail = projectEntry.thumbObj.file;
                            dbObj.filename = projectEntry.text;
                            dbObj.filetype_parent_id = 2;
                            dbObj.filetype_child_id = 0;
                            db.RBL_MediaMaster.Add(dbObj);
                        }
                    }
                }
                db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            
        }

        private String SaveFile(HttpPostedFileBase file)
        {
            try
            {
                if (file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var savedFilename = Guid.NewGuid().ToString() + fileName;
                    var path = Path.Combine(MediaModel.uploadDir, savedFilename);
                    file.SaveAs(path);
                    return savedFilename;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public string PDFToImage(string file, string outputPath, int dpi)
        {
            string path = HostingEnvironment.MapPath("~/bin");
            string filename;
            //string path = Path.GetDirectoryName(new StackTrace().GetFrames().Last().GetMethod().Module.Assembly.Location);
            try
            {
                Ghostscript.NET.Rasterizer.GhostscriptRasterizer rasterizer = null;
                Ghostscript.NET.GhostscriptVersionInfo vesion = new Ghostscript.NET.GhostscriptVersionInfo(new Version(0, 0, 0), path + @"/gsdll32.dll", string.Empty, Ghostscript.NET.GhostscriptLicense.GPL);

                using (rasterizer = new Ghostscript.NET.Rasterizer.GhostscriptRasterizer())
                {
                    rasterizer.Open(file, vesion, false);
                    filename = "thumb" + Guid.NewGuid().ToString() + Path.GetFileNameWithoutExtension(file) + ".jpg";
                    string pageFilePath = Path.Combine(outputPath, filename);

                    Image img = rasterizer.GetPage(dpi, dpi, 1);
                    img.Save(pageFilePath, ImageFormat.Jpeg);

                    rasterizer.Close();
                }
                return filename;
            }
            catch (Exception ex)
            {
                return null;
            }
            
        }

        public void ProcessRequest(HttpContext context)
        {
            this.currentUserId = Convert.ToInt64(context.Session["uid"]);
        }
         public bool IsReusable
        {
            get
            {
                return true;
            }
        }
        #endregion

        #endregion


        #region Editing Section

        // needed to removed all categories, subcategories, keywords location and language relations
        public void removeChildReferences(long id)
        {
            int noOfRowDeleted = db.Database.ExecuteSqlCommand("DELETE FROM [dbo].[RBL_MediaMasterCategory]  WHERE [media_master_id] = " +id);
            System.Diagnostics.Debug.WriteLine("deleted categories relationship " + noOfRowDeleted);
            noOfRowDeleted = db.Database.ExecuteSqlCommand("DELETE FROM [dbo].[RBL_MediaMasterKeyword]  WHERE [media_master_id] = " + id);
            System.Diagnostics.Debug.WriteLine("deleted keyword relationship " + noOfRowDeleted);
            noOfRowDeleted = db.Database.ExecuteSqlCommand("DELETE FROM [dbo].[RBL_MediaMasterLanguage]  WHERE [media_master_id] = " + id);
            System.Diagnostics.Debug.WriteLine("deleted language relationship " + noOfRowDeleted);
            noOfRowDeleted = db.Database.ExecuteSqlCommand("DELETE FROM [dbo].[RBL_MediaMasterLocation]  WHERE [media_master_id] = " + id);
            System.Diagnostics.Debug.WriteLine("deleted location relationship " + noOfRowDeleted);
        }

        public void updateMedia()
        {
            var dbObj = db.RBL_MediaMaster.Where(x => x.id == editModel.Id).FirstOrDefault();
            if (editModel.IsProject())
            {

                if (editModel.UploadedImage == null && editModel.imagelist == null)
                {
                    if (editModel.videoThumblist == null && editModel.UploadedVideo == null)
                    {
                        if (editModel.UploadedPdf == null && editModel.pdflist == null)
                        {
                            throw new Exception("you should try to inactive it");
                        }
                    }
                }
                
                dbObj.title = editModel.Title;
                dbObj.description = editModel.Description;
                var currentTime = DateTime.Now;
                dbObj.updated_at = currentTime;
                dbObj.updated_by = currentUserId;
                foreach (var fileType in editModel.SelectTypeProject)
                {
                    if (fileType == 1)
                    {
                        
                        //check to see if user has updated image or removed
                        if (editModel.UploadedImage != null)
                        {
                            var dbUploadedImage = db.RBL_MediaMaster.Where(x =>  x.parent_id == editModel.Id).Where(x => x.filetype_parent_id == 1).Select(x => x.id).ToList();
                            var removed = dbUploadedImage.Except(editModel.UploadedImage).ToList();
                            if (dbUploadedImage.Count() != editModel.UploadedImage.Count())
                            {
                                foreach (var item in removed)
                                {
                                    var id = Convert.ToInt64(item);
                                    var dbEntry = db.RBL_MediaMaster.Where(x => x.id == id).FirstOrDefault();
                                    if (dbEntry.filename != null)
                                    {
                                        System.IO.File.Delete(MediaModel.uploadDir + (string)dbEntry.filename);
                                    }
                                    db.Entry(dbEntry).State = System.Data.Entity.EntityState.Deleted;
                                    removeChildReferences(id);
                                }
                            }
                        }
                        if (editModel.imagelist != null)
                        {

                            var length = editModel.imagelist.Count();
                            var uploadedImages = new dynamic[length];
                            try
                            {
                                for (int i = 0; i < length; i++)
                                {
                                    var image = editModel.imagelist.ElementAt(i);
                                    if (image != null)
                                    {
                                        uploadedImages[i]= new { file = SaveFile(image),size = editModel.imagelist.ElementAt(i).ContentLength };
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                foreach (var item in uploadedImages)
                                {
                                    if (!String.IsNullOrEmpty(item))
                                    {
                                        System.IO.File.Delete(MediaModel.uploadDir + (string)item.file);
                                    }
                                }
                                throw new Exception("Couldnt Save files");
                            }
                            foreach (var item in uploadedImages)
                            {
                                if (item != null)
                                {
                                    RBL_MediaMaster nonProject = new RBL_MediaMaster();
                                    nonProject.user_master_id = currentUserId;
                                    nonProject.is_project = true;
                                    nonProject.parent_id = editModel.Id;
                                    nonProject.title = editModel.Title;
                                    nonProject.description = editModel.Description;
                                    nonProject.filetype_parent_id = 1;
                                    nonProject.filetype_child_id = 0;
                                    nonProject.guid = Guid.NewGuid().ToString();
                                    nonProject.is_approved = Helpers.Utility.isAdmin(currentUserId);
                                    currentTime = DateTime.Now;
                                    nonProject.created_by = currentUserId;
                                    nonProject.updated_by = currentUserId;
                                    nonProject.created_at = currentTime;
                                    nonProject.updated_at = currentTime;
                                    nonProject.thumbnail = item.file;
                                    nonProject.filename = item.file;
                                    nonProject.extension = Path.GetExtension(item.file);
                                    nonProject.size = ((int)item.size).ToString();
                                    db.RBL_MediaMaster.Add(nonProject);
                                }
                            }
                        }
                    }
                    else if (fileType == 2)
                    {
 
                        var length = editModel.UploadedVideo.Count();
                        for (int i = 0; i < length; i++)
                        {
                            var dbVideoObj = db.RBL_MediaMaster.Find(editModel.UploadedVideo.ElementAt(i));
                            if (dbVideoObj != null)
                            {
                                dbVideoObj.filename = editModel.UploadVideoText.ElementAt(i);
                            }
                        }

                        var dbUploadedVideos = db.RBL_MediaMaster.Where(x =>  x.parent_id == editModel.Id).Where(x => x.filetype_parent_id == 2).Select(x => x.id).ToList();
                        var removed = dbUploadedVideos.Except(editModel.UploadedVideo).ToList();
                        if (dbUploadedVideos.Count() != editModel.UploadedVideo.Count())
                        {
                            foreach (var item in removed)
                            {
                                var id = Convert.ToInt64(item);
                                var dbEntry = db.RBL_MediaMaster.Where(x => x.id == id).FirstOrDefault();
                                if (dbEntry.thumbnail != null)
                                {
                                    System.IO.File.Delete(MediaModel.uploadDir + (string)dbEntry.thumbnail);
                                }
                                db.Entry(dbEntry).State = System.Data.Entity.EntityState.Deleted;
                                removeChildReferences(id);
                            }
                        }

                        if (editModel.videoThumblist != null)
                        {
                            length = editModel.videoThumblist.Count();
                            var uploadedVideo = new dynamic[length];
                            try
                            {
                                for (int i = 0; i < length; i++)
                                {
                                    var item = editModel.videoThumblist.ElementAt(i);
                                    if (item != null)
                                    {
                                        var SavedName = SaveFile(item);
                                        if (SavedName != null)
                                        {
                                            uploadedVideo[i] = new { thumb = SavedName, fileSize = Convert.ToString(item.ContentLength), file = editModel.videolist.ElementAt(i), extension = Path.GetExtension(SavedName) };
                                        }
                                        else
                                            throw new Exception("could not upload file" + item.FileName);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                foreach (var item in uploadedVideo)
                                {
                                    if (!String.IsNullOrEmpty(item))
                                    {
                                        System.IO.File.Delete(MediaModel.uploadDir + (string)item.thumb);
                                    }
                                }
                                throw ex;
                            }

                            foreach (var item in uploadedVideo)
                            {
                                if (item !=null)
                                {
                                    RBL_MediaMaster nonProject = new RBL_MediaMaster();
                                    nonProject.user_master_id = currentUserId;
                                    nonProject.is_project = true;
                                    nonProject.parent_id = editModel.Id;
                                    nonProject.title = editModel.Title;
                                    nonProject.description = editModel.Description;
                                    nonProject.filetype_parent_id = 2;
                                    nonProject.filetype_child_id = 0;
                                    nonProject.guid = Guid.NewGuid().ToString();
                                    nonProject.is_approved = Helpers.Utility.isAdmin(currentUserId);
                                    currentTime = DateTime.Now;
                                    nonProject.created_by = currentUserId;
                                    nonProject.updated_by = currentUserId;
                                    nonProject.created_at = currentTime;
                                    nonProject.updated_at = currentTime;
                                    nonProject.thumbnail = item.thumb;
                                    nonProject.filename = item.file;
                                    nonProject.extension = item.extension;
                                    nonProject.size = item.fileSize;
                                    db.RBL_MediaMaster.Add(nonProject);
                                }
                            }


                        }


                    }
                    else if (fileType == 3)
                    {
                        var length = editModel.UploadedPdf.Count();
                        for (int i = 0; i < length; i++)
                        {
                            var dbVideoObj = db.RBL_MediaMaster.Find(editModel.UploadedPdf.ElementAt(i));
                            if (dbVideoObj != null)
                            {
                                dbVideoObj.filetype_child_id = editModel.uploadedPdfType.ElementAt(i);
                            }
                        }

                        var dbUploadedPdfs = db.RBL_MediaMaster.Where(x => x.parent_id == editModel.Id).Where(x => x.filetype_parent_id == 3).Select(x => x.id).ToList();
                        var removed = dbUploadedPdfs.Except(editModel.UploadedPdf).ToList();
                        if (dbUploadedPdfs.Count() != editModel.UploadedPdf.Count())
                        {
                            foreach (var item in removed)
                            {
                                var id = Convert.ToInt64(item);
                                var dbEntry = db.RBL_MediaMaster.Where(x => x.id == id).FirstOrDefault();
                                if (dbEntry.thumbnail != null)
                                {
                                    System.IO.File.Delete(MediaModel.uploadDir + (string)dbEntry.thumbnail);
                                }
                                if (dbEntry.filename != null)
                                {
                                    System.IO.File.Delete(MediaModel.uploadDir + (string)dbEntry.filename);
                                }
                                db.Entry(dbEntry).State = System.Data.Entity.EntityState.Deleted;
                                removeChildReferences(id);
                            }
                        }

                        if (editModel.pdflist != null)
                        {
                            length = editModel.pdflist.Count();
                            var uploadedPdfs = new dynamic[length];
                            try
                            {
                                for (int i = 0; i < length; i++)
                                {
                                    var item = editModel.pdflist.ElementAt(i);
                                    if (item != null)
                                    {
                                        var SavedName = SaveFile(item);
                                        var pdfthumb = PDFToImage(SavedName, MediaModel.uploadDir, 200);
                                        if (SavedName != null)
                                        {
                                            uploadedPdfs[i] = new { thumb = pdfthumb, fileSize = Convert.ToString(item.ContentLength), file = SavedName, extension = ".pdf", type = editModel.pdfType.ElementAt(0) };
                                        }
                                        else
                                            throw new Exception("could not upload file" + item.FileName);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                foreach (var item in uploadedPdfs)
                                {
                                    if (!String.IsNullOrEmpty(item))
                                    {
                                        System.IO.File.Delete(MediaModel.uploadDir + (string)item.thumb);
                                        System.IO.File.Delete(MediaModel.uploadDir + (string)item.file);
                                    }
                                }
                                throw ex;
                            }

                            foreach (var item in uploadedPdfs)
                            {
                                if (item != null)
                                {
                                    RBL_MediaMaster nonProject = new RBL_MediaMaster();
                                    nonProject.user_master_id = currentUserId;
                                    nonProject.is_project = true;
                                    nonProject.parent_id = editModel.Id;
                                    nonProject.title = editModel.Title;
                                    nonProject.description = editModel.Description;
                                    nonProject.filetype_parent_id = 3;
                                    nonProject.filetype_child_id = item.type;
                                    nonProject.guid = Guid.NewGuid().ToString();
                                    nonProject.is_approved = Helpers.Utility.isAdmin(currentUserId);
                                    currentTime = DateTime.Now;
                                    nonProject.created_by = currentUserId;
                                    nonProject.updated_by = currentUserId;
                                    nonProject.created_at = currentTime;
                                    nonProject.updated_at = currentTime;
                                    nonProject.thumbnail = item.thumb;
                                    nonProject.filename = item.file;
                                    nonProject.extension = item.extension;
                                    nonProject.size = item.fileSize;
                                    db.RBL_MediaMaster.Add(nonProject);
                                }
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("No File Type has Selected");
                    }
                }
                db.SaveChanges();
                var First = db.RBL_MediaMaster.Where(x => x.parent_id == dbObj.id).FirstOrDefault();
                if (First != null)
                {
                    if (First.filetype_parent_id == 1)
                    {
                        dbObj.thumbnail = First.filename;
                    }
                    else if (First.filetype_parent_id == 2 || First.filetype_parent_id == 3)
                    {
                        dbObj.thumbnail = First.thumbnail;
                    }
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("all the files have been deleted of project");
                }

            }
            else
            {
                dbObj.title = editModel.Title;
                dbObj.description = editModel.Description;
                var currentTime = DateTime.Now;
                dbObj.updated_at = currentTime;
                dbObj.updated_by = currentUserId;
                if (editModel.SelectTypeNonProject == "1")
                {

                    if (editModel.UploadedImage == null && editModel.imagelist == null)
                    {
                        throw new Exception("you should try to inactive it");
                    }

                    //check to see if user has updated image or removed
                    if (editModel.UploadedImage != null)
                    {
                        var dbUploadedImage = db.RBL_MediaMaster.Where(x => x.id == editModel.Id || x.parent_id == editModel.Id).Where(x => x.filetype_parent_id == 1).Select(x => x.id).ToList();
                        var removed = dbUploadedImage.Except(editModel.UploadedImage).ToList();
                        if (dbUploadedImage.Count() != editModel.UploadedImage.Count())
                        {
                            foreach (var item in removed)
                            {
                                var id = Convert.ToInt64(item);
                                var dbEntry = db.RBL_MediaMaster.Where(x => x.id == id).FirstOrDefault();
                                if (dbEntry.filename != null)
                                {
                                    System.IO.File.Delete(MediaModel.uploadDir + (string)dbEntry.filename);
                                }
                                db.Entry(dbEntry).State = System.Data.Entity.EntityState.Deleted;
                                removeChildReferences(id);
                            }
                        }
                    }
                    if (editModel.imagelist != null)
                    {

                        var length = editModel.imagelist.Count();
                        var uploadedImages = new dynamic[length];
                        try
                        {
                            for (int i = 0; i < length; i++)
                            {
                                var image = editModel.imagelist.ElementAt(i);
                                if (image != null)
                                {
                                    uploadedImages[i] = new { file = SaveFile(image), size = editModel.imagelist.ElementAt(i).ContentLength };
                                }
                            }
                        }
                        catch (Exception)
                        {
                            foreach (var item in uploadedImages)
                            {
                                if (!String.IsNullOrEmpty(item))
                                {
                                    System.IO.File.Delete(MediaModel.uploadDir + (string)item.file);
                                }
                            }                        
                            throw new Exception("Couldnt Save files");
                        }
                        foreach (var item in uploadedImages)
                        {
                            if (item != null)
                            {
                                RBL_MediaMaster nonProject = new RBL_MediaMaster();
                                nonProject.user_master_id = currentUserId;
                                nonProject.is_project = false;
                                nonProject.parent_id = editModel.Id;
                                nonProject.title = editModel.Title;
                                nonProject.description = editModel.Description;
                                nonProject.filetype_parent_id = 1;
                                nonProject.filetype_child_id = 0;
                                nonProject.guid = Guid.NewGuid().ToString();
                                nonProject.is_approved = Helpers.Utility.isAdmin(currentUserId);
                                currentTime = DateTime.Now;
                                nonProject.created_by = currentUserId;
                                nonProject.updated_by = currentUserId;
                                nonProject.created_at = currentTime;
                                nonProject.updated_at = currentTime;
                                nonProject.thumbnail = item.file;
                                nonProject.filename = item.file;
                                nonProject.extension = Path.GetExtension(item.file);
                                nonProject.size = ((long)item.size).ToString();
                                db.RBL_MediaMaster.Add(nonProject);
                            }
                        }
                    }
                }
                else if (editModel.SelectTypeNonProject == "2")
                {
                    if (editModel.videoThumblist == null && editModel.UploadedVideo == null)
                    {
                        throw new Exception("you should try to inactive it");
                    }
                    var length = editModel.UploadedVideo.Count();
                    for (int i = 0; i < length; i++)
                    {
                        var dbVideoObj = db.RBL_MediaMaster.Find(editModel.UploadedVideo.ElementAt(i));
                        if (dbVideoObj != null)
                        {
                            dbVideoObj.filename = editModel.UploadVideoText.ElementAt(i);
                        }
                    }

                    var dbUploadedVideos = db.RBL_MediaMaster.Where(x => x.id == editModel.Id || x.parent_id == editModel.Id).Where(x => x.filetype_parent_id == 2).Select(x => x.id).ToList();
                    var removed = dbUploadedVideos.Except(editModel.UploadedVideo).ToList();
                    if (dbUploadedVideos.Count() != editModel.UploadedVideo.Count())
                    {
                        foreach (var item in removed)
                        {
                            var id = Convert.ToInt64(item);
                            var dbEntry = db.RBL_MediaMaster.Where(x => x.id == id).FirstOrDefault();
                            if (dbEntry.thumbnail != null)
                            {
                                System.IO.File.Delete(MediaModel.uploadDir + (string)dbEntry.thumbnail);
                            }
                            db.Entry(dbEntry).State = System.Data.Entity.EntityState.Deleted;
                            removeChildReferences(id);
                        }
                    }

                    if (editModel.videoThumblist != null)
                    {
                        length = editModel.videoThumblist.Count();
                        var uploadedVideo = new dynamic[length];
                        try
                        {
                            for (int i = 0; i < length; i++)
                            {
                                var item = editModel.videoThumblist.ElementAt(i);
                                if (item !=null)
                                {
                                    var SavedName = SaveFile(item);
                                    if (SavedName != null)
                                    {
                                        uploadedVideo[i] = new { thumb = SavedName, fileSize = Convert.ToString(item.ContentLength), file = editModel.videolist.ElementAt(i), extension = Path.GetExtension(SavedName) };
                                    }
                                    else
                                        throw new Exception("could not upload file" + item.FileName);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            foreach (var item in uploadedVideo)
                            {
                                if (!String.IsNullOrEmpty(item))
                                {
                                    System.IO.File.Delete(MediaModel.uploadDir + (string)item.thumb);
                                }
                            }
                            throw ex;
                        }

                        foreach (var item in uploadedVideo)
                        {
                            if (item != null)
                            {
                                RBL_MediaMaster nonProject = new RBL_MediaMaster();
                                nonProject.user_master_id = currentUserId;
                                nonProject.is_project = false;
                                nonProject.parent_id = editModel.Id;
                                nonProject.title = editModel.Title;
                                nonProject.description = editModel.Description;
                                nonProject.filetype_parent_id = 2;
                                nonProject.filetype_child_id = 0;
                                nonProject.guid = Guid.NewGuid().ToString();
                                nonProject.is_approved = Helpers.Utility.isAdmin(currentUserId);
                                currentTime = DateTime.Now;
                                nonProject.created_by = currentUserId;
                                nonProject.updated_by = currentUserId;
                                nonProject.created_at = currentTime;
                                nonProject.updated_at = currentTime;
                                nonProject.thumbnail = item.thumb;
                                nonProject.filename = item.file;
                                nonProject.extension = item.extension;
                                nonProject.size = item.fileSize;
                                db.RBL_MediaMaster.Add(nonProject);
                            }
                        }


                    }


                }
                else if (editModel.SelectTypeNonProject == "3")
                {
                    if (editModel.UploadedVideo == null && editModel.videolist == null)
                    {
                        throw new Exception("you should try to inactive it");
                    }
                    var length = editModel.UploadedPdf.Count();
                    for (int i = 0; i < length; i++)
                    {
                        var dbVideoObj = db.RBL_MediaMaster.Find(editModel.UploadedPdf.ElementAt(i));
                        if (dbVideoObj != null)
                        {
                            dbVideoObj.filetype_child_id = editModel.uploadedPdfType.ElementAt(i);
                        }
                    }

                    var dbUploadedPdfs = db.RBL_MediaMaster.Where(x => x.id == editModel.Id || x.parent_id == editModel.Id).Where(x => x.filetype_parent_id == 3).Select(x => x.id).ToList();
                    var removed = dbUploadedPdfs.Except(editModel.UploadedPdf).ToList();
                    if (dbUploadedPdfs.Count() != editModel.UploadedPdf.Count())
                    {
                        foreach (var item in removed)
                        {
                            var id = Convert.ToInt64(item);
                            var dbEntry = db.RBL_MediaMaster.Where(x => x.id == id).FirstOrDefault();
                            if (dbEntry.thumbnail != null)
                            {
                                System.IO.File.Delete(MediaModel.uploadDir + (string)dbEntry.thumbnail);
                            }
                            if (dbEntry.filename != null)
                            {
                                System.IO.File.Delete(MediaModel.uploadDir + (string)dbEntry.filename);
                            }

                            db.Entry(dbEntry).State = System.Data.Entity.EntityState.Deleted;
                            removeChildReferences(id);
                        }
                    }

                    if (editModel.pdflist != null)
                    {
                        length = editModel.pdflist.Count();
                        var uploadedPdfs = new dynamic[length];
                        try
                        {
                            for (int i = 0; i < length; i++)
                            {
                                var item = editModel.pdflist.ElementAt(i);
                                if (item != null)
                                {
                                    var SavedName = SaveFile(item);
                                    var pdfthumb = PDFToImage(MediaModel.uploadDir+SavedName, MediaModel.uploadDir, 200);
                                    if (SavedName != null)
                                    {
                                        uploadedPdfs[i] = new { thumb = pdfthumb, fileSize = Convert.ToString(item.ContentLength), file = SavedName, extension = ".pdf", type = editModel.pdfType.ElementAt(0) };
                                    }
                                    else
                                        throw new Exception("could not upload file" + item.FileName);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            foreach (var item in uploadedPdfs)
                            {
                                if (!String.IsNullOrEmpty(item))
                                {
                                    System.IO.File.Delete(MediaModel.uploadDir + (string)item.thumb);
                                    System.IO.File.Delete(MediaModel.uploadDir + (string)item.file);
                                }
                            }
                            throw ex;
                        }

                        foreach (var item in uploadedPdfs)
                        {
                            if (item != null)
                            {
                                RBL_MediaMaster nonProject = new RBL_MediaMaster();
                                nonProject.user_master_id = currentUserId;
                                nonProject.is_project = false;
                                nonProject.parent_id = editModel.Id;
                                nonProject.title = editModel.Title;
                                nonProject.description = editModel.Description;
                                nonProject.filetype_parent_id = 3;
                                nonProject.filetype_child_id = item.type;
                                nonProject.guid = Guid.NewGuid().ToString();
                                nonProject.is_approved = Helpers.Utility.isAdmin(currentUserId);
                                currentTime = DateTime.Now;
                                nonProject.created_by = currentUserId;
                                nonProject.updated_by = currentUserId;
                                nonProject.created_at = currentTime;
                                nonProject.updated_at = currentTime;
                                nonProject.thumbnail = item.thumb;
                                nonProject.filename = item.file;
                                nonProject.extension = item.extension;
                                nonProject.size = item.fileSize;
                                db.RBL_MediaMaster.Add(nonProject);

                            }
                        }
                    }
                }
                else
                {
                    throw new Exception("No File Type has Selected");
                }

                db.SaveChanges();
                var mainDBEntry = db.RBL_MediaMaster.Where(x => x.id == editModel.Id).Select(x=>(long?)x.id).FirstOrDefault();
                if (mainDBEntry == null)
                {
                    //db.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[RBL_MediaMaster] ON");
                    var firstEntry = db.RBL_MediaMaster.Where(x => x.parent_id == editModel.Id).FirstOrDefault();
                    //var restoreEntry = new RBL_MediaMaster();
                    //restoreEntry.id = editModel.Id;
                    //restoreEntry.user_master_id = firstEntry.user_master_id;
                    //restoreEntry.guid = firstEntry.guid;
                    //restoreEntry.title = firstEntry.title;
                    //restoreEntry.description = firstEntry.description;
                    //restoreEntry.thumbnail = firstEntry.thumbnail;
                    //restoreEntry.filetype_child_id = firstEntry.filetype_child_id;
                    //restoreEntry.filetype_parent_id = firstEntry.filetype_parent_id;
                    //restoreEntry.filename = firstEntry.filename;
                    //restoreEntry.size = firstEntry.size;
                    //restoreEntry.is_public= firstEntry.is_public;
                    //restoreEntry.is_project = false;
                    //restoreEntry.extension= firstEntry.extension;
                    //restoreEntry.parent_id = 0;
                    //restoreEntry.download_count = firstEntry.download_count;
                    //restoreEntry.is_approved= firstEntry.is_approved;
                    //restoreEntry.is_active= firstEntry.is_active;
                    //restoreEntry.created_by= firstEntry.created_by;
                    //restoreEntry.created_at= firstEntry.created_at;
                    //restoreEntry.updated_by = firstEntry.updated_by;
                    //restoreEntry.updated_at= firstEntry.updated_at;
                    //restoreEntry.deleted_by= firstEntry.deleted_by;
                    //restoreEntry.deleted_at= firstEntry.deleted_at;
                    //restoreEntry.delete_status= firstEntry.delete_status;
                    //db.RBL_MediaMaster.Add(restoreEntry);
                    var list = db.RBL_MediaMaster.Where(x => x.parent_id == editModel.Id).ToList();
                    list.ForEach(m => m.parent_id = firstEntry.id);
                    db.SaveChanges();
                    firstEntry.parent_id = 0;
                    editModel.Id = firstEntry.id;
                    parentId = editModel.Id;
                    //db.Database.ExecuteSqlCommand("SET IDENTITY_INSERT [dbo].[RBL_MediaMaster] OFF");
                    //db.Entry(firstEntry).State = System.Data.Entity.EntityState.Deleted;
                    db.SaveChanges();
                }
                
            }

            parentId = editModel.Id;
            SaveTaxonomy(true);
            db.SaveChanges();
        }

        #endregion
    }

}