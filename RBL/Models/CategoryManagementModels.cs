﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RBL.Models
{
    public class CategoryModel{

        private ParentCategoryAddViewModel model;
        private mobisnuu_rblEntities db = new mobisnuu_rblEntities();

        private long currentUserId = Convert.ToInt64(HttpContext.Current.Session["uid"]);

        public static List<ParentCategoryViewModel> getParentCategory() {
            using (var db = new mobisnuu_rblEntities())
            {
                var list = db.RBL_Category.Where(x=>x.delete_status == false).Where(x => x.parent_id == 0).ToList();
                List<ParentCategoryViewModel> viewmodellist = new List<ParentCategoryViewModel>();
                foreach (var item in list)
                {
                    viewmodellist.Add(
                        new ParentCategoryViewModel { Name = item.name, Id = item.id, Created_At = item.created_at, isActive = item.is_active }
                        );
                }
                return viewmodellist;
            }
        }

        public static List<ChildCategoryViewModel> getChildCategory(long ParentId = 0)
        {

            using (var db = new mobisnuu_rblEntities())
            {
                var listObj = db.RBL_Category.Where(x => x.delete_status == false).Join(db.RBL_Category.Where(x => x.delete_status == false), m=>m.id,j=>j.parent_id,(r,j) => new ChildCategoryViewModel { ParentName = r.name, SubName = j.name, Image = j.image, Discription =j.description , Date = j.created_at ,Id = j.id, isActive = j.is_active , parent_id =r.id});
                if (ParentId != 0)
                    listObj.Where(x => x.parent_id != 0);
                else
                    listObj.Where(x => x.parent_id == ParentId);
                var list = listObj.ToList();
                return list;
            }
        }

        public CategoryModel(ParentCategoryAddViewModel model)
        {
            this.model = model;
        }

        public  bool SaveCategory() {
            try
            {
                var catObj = db.RBL_Category.Where(x => x.name.ToLower() == model.Name.ToLower()).FirstOrDefault();
                if (catObj == null)
                {
                    catObj = new RBL_Category();
                    catObj.name = model.Name;
                    catObj.created_by = currentUserId;
                    catObj.updated_by = currentUserId;
                    DateTime? currentTime = DateTime.Now;
                    catObj.created_at = currentTime;
                    catObj.updated_at = currentTime;
                    catObj.guid = Guid.NewGuid().ToString();
                    catObj.parent_id = 0;
                    catObj.description = model.Name;
                    db.RBL_Category.Add(catObj);
                    db.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}