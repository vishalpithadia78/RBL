﻿using RBL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace RBL
{
    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute(), 2); //by default added


            filters.Add(new HandleErrorAttribute
            {
                View = "Error"
            }, 1);
        }

        protected void Application_Start()
        {
            MvcHandler.DisableMvcResponseHeader = true;
            RegisterGlobalFilters(GlobalFilters.Filters);
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
        protected void Session_End(object sender, EventArgs e)
        {
            try
            {
                FormsAuthentication.SignOut();
            }
            catch (Exception)
            {
               //keep going
            }
            using (var db = new mobisnuu_rblEntities())
            {
                var id = (long?)Session["uid"];
                var dbUser = db.RBL_UserMaster.Where(x => x.id == id).FirstOrDefault();
                if (dbUser != null)
                {
                    if (string.IsNullOrEmpty(dbUser.token_key))
                        return;
                    if (Session.SessionID != dbUser.token_key)
                        return;
                    dbUser.token_key = string.Empty;
                    db.SaveChanges();
                }
            }
            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();

        }

        protected void Session_Start(object sender, EventArgs e)
        {


            // event is raised each time a new session is created     
        }

    }
}
