﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using RBL.Models;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Net;
using System.Net.Mail;
using System.Configuration;
using System.Runtime.InteropServices;

namespace RBL.Helpers
{
    public class Constant
    {
        public static string version { get; set; } = "v1.5.3.10";
    }
    public class Utility {

        public static string imageBaseUrl = GetBaseUrl() + "/App_Data/uploads/";
        public static string GetBaseUrl()
        {
            var request = HttpContext.Current.Request;
            var appUrl = HttpRuntime.AppDomainAppVirtualPath;

            if (appUrl != "/")
                appUrl = "/" + appUrl;

            var baseUrl = string.Format("{0}://{1}{2}", request.Url.Scheme, request.Url.Authority, appUrl);

            return baseUrl;
        }
        public static string FormatNumber(long num)
        {
            // Ensure number has max 3 significant digits (no rounding up can happen)
            long i = (long)Math.Pow(10, (int)Math.Max(0, Math.Log10(num) - 2));
            num = num / i * i;

            if (num >= 1000000000)
                return (num / 1000000000D).ToString("0.##") + "B";
            if (num >= 1000000)
                return (num / 1000000D).ToString("0.##") + "M";
            if (num >= 1000)
                return (num / 1000D).ToString("0.##") + "K";

            return num.ToString("#,0");
        }

        public static bool isAdmin(long id) {
            using (var db = new mobisnuu_rblEntities()) {
                try
                {
                    var dbObj = db.RBL_UserMaster.Where(x => x.id == id).Where(x=>x.role_id == 1).FirstOrDefault();
                    if (dbObj != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }
        public static bool IsPropertyExist(dynamic settings, string name)
        {
            return settings.GetType().GetProperty(name) != null;
        }

        public static bool turnOffAuth() {
            return false;
        }
        public static string fromEmail()
        {
            string fromEmail = String.Empty;
            fromEmail = ConfigurationManager.AppSettings["fromEmail"];
            return fromEmail;
        }
        public static string fromEmailName()
        {
            string fromEmailName = String.Empty;
            fromEmailName = ConfigurationManager.AppSettings["fromEmailName"];
            return fromEmailName;
        }
        public static string fromEmailPassword()
        {
            string fromEmailPassword = String.Empty;
            fromEmailPassword = ConfigurationManager.AppSettings["fromEmailPassword"];
            return fromEmailPassword;
        }
        public static string smtpHost()
        {
            string smtpHost = String.Empty;
            smtpHost = ConfigurationManager.AppSettings["smtpHost"];
            return smtpHost;
        }
        public static int smtpPort()
        {
            string smtpPort = String.Empty;
            smtpPort = ConfigurationManager.AppSettings["smtpPort"];
            return Convert.ToInt32(smtpPort);
        }
        public static bool smtpEnableSsl()
        {
            string smtpEnableSsl = String.Empty;
            smtpEnableSsl = ConfigurationManager.AppSettings["smtpEnableSsl"];
            return Convert.ToBoolean(smtpEnableSsl);
        }
        public static bool SendEmail(string toEmailAddress, string subject, string body)
        {
            bool mailSent = false;
            var fromAddress = new MailAddress(fromEmail(), fromEmailName());
            var toAddress = new MailAddress(toEmailAddress);
            string fromPassword = fromEmailPassword();
            var smtp = new SmtpClient
            {
                Host = smtpHost(),
                Port = smtpPort(),
                EnableSsl = smtpEnableSsl(),
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };
            using (var message = new MailMessage(fromAddress, toAddress)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
            {
                smtp.Send(message);
                mailSent = true;
            }
            return mailSent;
        }
        
    }
    public static class Helpers
    {
        public static IHtmlString CheckboxGroup<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> propertySelector, int value) where TProperty : IEnumerable<int>
        {
            var groupName = GetPropertyName(propertySelector);
            var modelValues = propertySelector.Compile().Invoke(htmlHelper.ViewData.Model);

            var svalue = value.ToString();
            var builder = new TagBuilder("input");
            builder.GenerateId(groupName);
            builder.Attributes.Add("type", "checkbox");
            builder.Attributes.Add("name", groupName);
            builder.Attributes.Add("value", svalue);
            var contextValues = HttpContext.Current.Request.Form.GetValues(groupName);
            if ((contextValues != null && contextValues.Contains(svalue)) || (modelValues != null && modelValues.Contains(value)))
            {
                builder.Attributes.Add("checked", null);
            }
            return new MvcHtmlString(builder.ToString(TagRenderMode.Normal));
        }

        private static string GetPropertyName<T, TProperty>(Expression<Func<T, TProperty>> propertySelector)
        {
            var body = propertySelector.Body.ToString();
            var firstIndex = body.IndexOf('.') + 1;
            return body.Substring(firstIndex);
        }

        public static string ComputeHash(string input, HashAlgorithm algorithm,string saltString)
        {
            Byte[] inputBytes = Encoding.UTF8.GetBytes(input);
            Byte[] salt = Encoding.UTF8.GetBytes(saltString);
            // Combine salt and input bytes
            Byte[] saltedInput = new Byte[salt.Length + inputBytes.Length];
            salt.CopyTo(saltedInput, 0);
            inputBytes.CopyTo(saltedInput, salt.Length);

            Byte[] hashedBytes = algorithm.ComputeHash(saltedInput);

            return BitConverter.ToString(hashedBytes);
        }
    }



    public class FileType
    {
        [DllImport(@"urlmon.dll", CharSet = CharSet.Auto)]
        private extern static System.UInt32 FindMimeFromData(
            System.UInt32 pBC,
            [MarshalAs(UnmanagedType.LPStr)] System.String pwzUrl,
            [MarshalAs(UnmanagedType.LPArray)] byte[] pBuffer,
            System.UInt32 cbSize,
            [MarshalAs(UnmanagedType.LPStr)] System.String pwzMimeProposed,
            System.UInt32 dwMimeFlags,
            out System.UInt32 ppwzMimeOut,
            System.UInt32 dwReserverd
        );

        public static string getMimeFromFile(string filename)
        {
            if (!File.Exists(filename))
                throw new FileNotFoundException(filename + " not found");

            byte[] buffer = new byte[256];
            using (FileStream fs = new FileStream(filename, FileMode.Open))
            {
                if (fs.Length >= 256)
                    fs.Read(buffer, 0, 256);
                else
                    fs.Read(buffer, 0, (int)fs.Length);
            }
            try
            {
                System.UInt32 mimetype;
                FindMimeFromData(0, null, buffer, 256, null, 0, out mimetype, 0);
                System.IntPtr mimeTypePtr = new IntPtr(mimetype);
                string mime = Marshal.PtrToStringUni(mimeTypePtr);
                Marshal.FreeCoTaskMem(mimeTypePtr);
                return mime;
            }
            catch (Exception e)
            {
                return "unknown/unknown";
            }
        }

        public static bool ValidateFileType(HttpPostedFileBase file)
        {
            Dictionary<string, byte[]> imageHeader = new Dictionary<string, byte[]>();
            imageHeader.Add("JPG", new byte[] { 0xFF, 0xD8, 0xFF, 0xE0 });
            imageHeader.Add("JPEG", new byte[] { 0xFF, 0xD8, 0xFF, 0xE0 });
            imageHeader.Add("PNG", new byte[] { 0x89, 0x50, 0x4E, 0x47 });
            imageHeader.Add("TIF", new byte[] { 0x49, 0x49, 0x2A, 0x00 });
            imageHeader.Add("TIFF", new byte[] { 0x49, 0x49, 0x2A, 0x00 });
            imageHeader.Add("GIF", new byte[] { 0x47, 0x49, 0x46, 0x38 });
            imageHeader.Add("BMP", new byte[] { 0x42, 0x4D });
            imageHeader.Add("ICO", new byte[] { 0x00, 0x00, 0x01, 0x00 });
            imageHeader.Add("PDF", new byte[] { 0x25, 0x50, 0x44, 0x46 });

            byte[] header;
            if (file != null)
            {
                string fileExt;
                fileExt = file.FileName.Substring(file.FileName.LastIndexOf('.') + 1).ToUpper();
                // CUSTOM VALIDATION GOES HERE BASED ON FILE EXTENSION IF ANY

                byte[] tmp = imageHeader[fileExt];
                header = new byte[tmp.Length];

                file.InputStream.Read(header, 0, header.Length);
                if (CompareArray(tmp, header))
                    return true;
                else
                    return false;
            }
            else
                return false;

        }
        private static bool CompareArray(byte[] a1, byte[] a2)
        {
            if (a1.Length != a2.Length)
                return false;

            for (int i = 0; i < a1.Length; i++)
            {
                if (a1[i] != a2[i])
                    return false;
            }

            return true;
        }

    }

    public class ListFileExtensionAttribute : ValidationAttribute
    {
        public ListFileExtensionAttribute(string extensions)
        {
            this.Extensions = extensions.Split(','); 
        }
        
        public string[] Extensions { get; private set; }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {

            var listOfFiles = value as IEnumerable<HttpPostedFileBase>;
            if (listOfFiles != null && listOfFiles.ElementAt(0) != null)
            {
                bool isValid = true;
                string filename = string.Empty;
                foreach (var file in listOfFiles)
                {
                    var extension = Path.GetExtension(file.FileName);
                    if( !(this.Extensions.Contains(extension.ToLower())) || !FileType.ValidateFileType(file))
                    {
                        isValid = false;
                        break;
                    }
                }
                if (!isValid)
                {
                    return new ValidationResult(this.FormatErrorMessage(this.ErrorMessage));
                }
                return ValidationResult.Success;
            }
            return null;
        }
    }
}