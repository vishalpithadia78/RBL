﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RBL.Models;
using System.Web.Security;
using RBL.Helpers;

namespace RBL.Controllers
{
    [NoCache]
    public class BaseController : Controller
    {
        protected override void OnActionExecuting
            (ActionExecutingContext filterContext)
        {
            // If session exists
            if (filterContext.HttpContext.Session != null)
            {

                var uid = (long?)filterContext.HttpContext.Session["uid"];
                if (uid != null)
                {
                    using (var db = new mobisnuu_rblEntities())
                    {
                        var dbUser = db.RBL_UserMaster.Where(x => x.id == uid).FirstOrDefault();
                        if (dbUser != null)
                        {
                            if (!string.IsNullOrEmpty(dbUser.token_key))
                            {
                                string currentUserIpAddress = Request.ServerVariables[string.IsNullOrEmpty(Request.ServerVariables["HTTP_X_FORWARDED_FOR"]) ? "REMOTE_ADDR" : "HTTP_X_FORWARDED_FOR"].Trim();
                                string currentBrowserInfo = Request.Browser.Browser.Trim() + Request.Browser.Version.Trim() + Request.UserAgent.Trim();
                                string currentClientInfo = currentUserIpAddress + currentBrowserInfo;
                                string sessionCurrentClientInfo = (string) Session["currentClientInfo"];
                                if (filterContext.HttpContext.Session.SessionID != dbUser.token_key || currentClientInfo != sessionCurrentClientInfo)
                                {
                                    if (filterContext.HttpContext.Request.Cookies["ASP.NET_SessionId"] != null)
                                    {
                                        filterContext.HttpContext.Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                                        filterContext.HttpContext.Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
                                    }
                                    if (filterContext.HttpContext.Request.Cookies["DotNetOpenAuth.WebServerClient.XSRF-Session"] != null)
                                    {
                                        filterContext.HttpContext.Response.Cookies["DotNetOpenAuth.WebServerClient.XSRF-Session"].Value = string.Empty;
                                        filterContext.HttpContext.Response.Cookies["DotNetOpenAuth.WebServerClient.XSRF-Session"].Expires = DateTime.Now.AddMonths(-20);
                                    }
                                    Session.Abandon();
                                    FormsAuthentication.SignOut();
                                    dbUser.token_key = null;
                                    db.SaveChanges();
                                    filterContext.Result = RedirectToAction("Login", "Account");
                                    return;
                                }

                            }

                        }
                    }
                }

                //if new session
                //if (filterContext.HttpContext.Session.IsNewSession)
                //{
                //    string cookie =
                //        filterContext.HttpContext.Request.Headers["Cookie"];
                //    //if cookie exists and sessionid index is greater than zero
                //    if ((cookie != null) &&
                //        (cookie.IndexOf("ASP.NET_SessionId") >= 0))
                //    {
                //        //redirect to desired session 
                //        //expiration action and controller
                //        filterContext.Result =
                //            RedirectToAction("Login", "Account");
                //        return;
                //    }
                //}
            }
            //otherwise continue with action
            base.OnActionExecuting(filterContext);
        }
    }
}