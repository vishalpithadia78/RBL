﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RBL.Models;
using System.Net;
using RBL.Enums;
using System.Security.Cryptography;

namespace RBL.Controllers
{
    [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
    [Authorize]
    [HandleError(View = "Error")]
    public class UserController : BaseController
    {
        // GET: User
        public ActionResult Index()
        {
            var prms = (long?[])Session["permission"];
            if (RBL.Helpers.Utility.turnOffAuth() || (prms == null))
                return RedirectToAction("Login", "Account");
            if (Helpers.Utility.turnOffAuth() || !(prms.Contains((int)Permission.UserEdit) || prms.Contains((int)Permission.UserAdd) || prms.Contains((int)Permission.UserView)))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            var msg = (Session["msg"] as string);
            if ( msg != null)
            {
                ModelState.AddModelError("", msg);
                Session.Remove("msg");
            }
            using (var db = new mobisnuu_rblEntities())
            {
                var list = db.RBL_UserMaster.Join(db.RBL_RoleName, u => u.role_id, r => r.id, (u, r) => new UserList { Id = u.id,Role = r.role_name, Email = u.email, EmpCode = u.emp_code, Mobile = u.phone_no1, Name = u.name, IsActive = u.is_active}).ToList();
                return View(list);
            }
        }


        [HttpGet]
        public ActionResult Create()
        {
            var prms = (long?[])Session["permission"];
            if (RBL.Helpers.Utility.turnOffAuth() || (prms == null))
                return RedirectToAction("Login", "Account");
            if (Helpers.Utility.turnOffAuth() || !prms.Contains((int)Permission.UserAdd))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            using (var db = new mobisnuu_rblEntities())
            {
                var model = new UserComman();
                model.RoleList = db.RBL_RoleName.Where(x=>x.is_active == true).Select(x => new ListItemRoles { Id = x.id, RoleName = x.role_name }).ToList();
                var allRolePermission = db.RBL_RolePermission.Select(x => new { Id = x.id, RoleId = x.role_id,MenuId = x.menu_id }).ToList();
                foreach (var item in model.RoleList)
                {
                    var permissions = allRolePermission.Where(x => x.RoleId == item.Id).Select(x => (long)x.MenuId).ToList();
                    item.permissions = permissions;
                }
                var categorylist = db.RBL_Menu.Where(x => x.parent_id == 2).Select(x => new { Id = x.id, Type = x.action_name }).ToList();
                var temp = new List<KeyValuePair>();
                foreach (var item in categorylist)
                {
                    var id = System.Convert.ToString(item.Id);
                    temp.Add(new KeyValuePair { Id = id, Type = item.Type });
                }
                model.CategoryPermission = temp;
                var filelist = db.RBL_Menu.Where(x => x.parent_id == 5).Select(x => new { Id = x.id, Type = x.action_name }).ToList();
                temp = new List<KeyValuePair>();
                foreach (var item in filelist)
                {
                    var id = System.Convert.ToString(item.Id);
                    temp.Add(new KeyValuePair { Id = id, Type = item.Type });
                }
                model.FilePermission = temp;
                var userlist = db.RBL_Menu.Where(x => x.parent_id == 8).Select(x => new { Id = x.id, Type = x.action_name }).ToList();
                temp = new List<KeyValuePair>();
                foreach (var item in userlist)
                {
                    var id = System.Convert.ToString(item.Id);
                    temp.Add(new KeyValuePair { Id = id, Type = item.Type });
                }
                model.UserPermission = temp;
                return View(model);
            }            
        }


        /// <summary>
        ///  im writing this code at 3:30 am do you think i care about 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UserComman model)
        {
            var prms = (long?[])Session["permission"];
            if (RBL.Helpers.Utility.turnOffAuth() || (prms == null))
                return RedirectToAction("Login", "Account");
            if (Helpers.Utility.turnOffAuth() || !prms.Contains((int)Permission.UserAdd))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            
            using (var db = new mobisnuu_rblEntities())
            {
                model.RoleList = db.RBL_RoleName.Where(x => x.is_active == true).Select(x => new ListItemRoles { Id = x.id, RoleName = x.role_name }).ToList();
                var categorylist = db.RBL_Menu.Where(x => x.parent_id == 2).Select(x => new { Id = x.id, Type = x.action_name }).ToList();
                var allRolePermission = db.RBL_RolePermission.Select(x => new { Id = x.id, RoleId = x.role_id, MenuId = x.menu_id }).ToList();
                foreach (var item in model.RoleList)
                {
                    var permissions = allRolePermission.Where(x => x.RoleId == item.Id).Select(x => (long)x.MenuId).ToList();
                    item.permissions = permissions;
                }
                var temp = new List<KeyValuePair>();
                foreach (var item in categorylist)
                {
                    var id = System.Convert.ToString(item.Id);
                    temp.Add(new KeyValuePair { Id = id, Type = item.Type });
                }
                model.CategoryPermission = temp;
                var filelist = db.RBL_Menu.Where(x => x.parent_id == 5).Select(x => new { Id = x.id, Type = x.action_name }).ToList();
                temp = new List<KeyValuePair>();
                foreach (var item in filelist)
                {
                    var id = System.Convert.ToString(item.Id);
                    temp.Add(new KeyValuePair { Id = id, Type = item.Type });
                }
                model.FilePermission = temp;
                var userlist = db.RBL_Menu.Where(x => x.parent_id == 8).Select(x => new { Id = x.id, Type = x.action_name }).ToList();
                temp = new List<KeyValuePair>();
                foreach (var item in userlist)
                {
                    var id = System.Convert.ToString(item.Id);
                    temp.Add(new KeyValuePair { Id = id, Type = item.Type });
                }
                model.UserPermission = temp;
                if (ModelState.IsValid)
                {
                    var sameObj = db.RBL_UserMaster.Where(x => x.email == model.Email || x.phone_no1 == model.Mobile || x.phone_no2 == model.Mobile || x.emp_code == model.EmpCode).FirstOrDefault();
                    var Role = db.RBL_RoleName.Where(x => x.id == model.Role).FirstOrDefault();

                    if (sameObj == null && Role !=null && Role.is_active == true)
                    {
                        var currentTime = DateTime.Now;
                        var currentUserId = (long?)Session["uid"];
                        var newUser = new RBL_UserMaster();
                        newUser.name = model.Name;
                        newUser.email = model.Email;
                        newUser.phone_no1 = model.Mobile;
                        newUser.phone_no2 = model.Mobile;
                        newUser.emp_code = model.EmpCode;
                        newUser.guid = Guid.NewGuid().ToString();
                        newUser.verification_hash = Guid.NewGuid().ToString();
                        newUser.verification_status = true;
                        newUser.is_active = true;
                        newUser.role_id = model.Role;
                        newUser.password = Helpers.Helpers.ComputeHash(model.password, new SHA256CryptoServiceProvider(), currentTime.ToString());
                        newUser.updated_at = currentTime;
                        newUser.created_at = currentTime;
                        newUser.updated_by = currentUserId;
                        newUser.created_by = currentUserId;
                        db.RBL_UserMaster.Add(newUser);
                        db.SaveChanges();
                        Session["msg"] = "Succefully created user " + newUser.name ;
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        if (model.Email == sameObj.email)
                            ModelState.AddModelError("", "Same Email exists");
                        if (sameObj.phone_no1 == model.Mobile || sameObj.phone_no2 == model.Mobile)
                            ModelState.AddModelError("", "Same phone exists");
                        if (sameObj.emp_code == model.EmpCode)
                            ModelState.AddModelError("", "Same emp code exists");
                        if (Role == null)
                            ModelState.AddModelError("", "No Role Found for this id");
                        if (Role != null && Role.is_active == false)
                            ModelState.AddModelError("", "Role is not active");
                    }
                }
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult Edit(long? id)
        {
            var prms = (long?[])Session["permission"];
            if (RBL.Helpers.Utility.turnOffAuth() || (prms == null))
                return RedirectToAction("Login", "Account");
            if (Helpers.Utility.turnOffAuth() || !prms.Contains((int)Permission.UserEdit))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

           
            if (id != null)
            {
                using (var db = new mobisnuu_rblEntities())
                {
                    var dbObj = db.RBL_UserMaster.Where(x => x.id == id).FirstOrDefault();
                    if (dbObj !=null)
                    {
                        var model = new UserEdit();
                        model.RoleList = db.RBL_RoleName.Where(x => x.is_active == true).Select(x => new ListItemRoles { Id = x.id, RoleName = x.role_name }).ToList();
                        var categorylist = db.RBL_Menu.Where(x => x.parent_id == 2).Select(x => new { Id = x.id, Type = x.action_name }).ToList();
                        var allRolePermission = db.RBL_RolePermission.Select(x => new { Id = x.id, RoleId = x.role_id, MenuId = x.menu_id }).ToList();
                        foreach (var item in model.RoleList)
                        {
                            var permissions = allRolePermission.Where(x => x.RoleId == item.Id).Select(x => (long)x.MenuId).ToList();
                            item.permissions = permissions;
                        }
                        var temp = new List<KeyValuePair>();
                        foreach (var item in categorylist)
                        {
                            var tempid = System.Convert.ToString(item.Id);
                            temp.Add(new KeyValuePair { Id = tempid, Type = item.Type });
                        }
                        model.CategoryPermission = temp;
                        var filelist = db.RBL_Menu.Where(x => x.parent_id == 5).Select(x => new { Id = x.id, Type = x.action_name }).ToList();
                        temp = new List<KeyValuePair>();
                        foreach (var item in filelist)
                        {
                            var tempid = System.Convert.ToString(item.Id);
                            temp.Add(new KeyValuePair { Id = tempid, Type = item.Type });
                        }
                        model.FilePermission = temp;
                        var userlist = db.RBL_Menu.Where(x => x.parent_id == 8).Select(x => new { Id = x.id, Type = x.action_name }).ToList();
                        temp = new List<KeyValuePair>();
                        foreach (var item in userlist)
                        {
                            var tempid = System.Convert.ToString(item.Id);
                            temp.Add(new KeyValuePair { Id = tempid, Type = item.Type });
                        }
                        model.UserPermission = temp;
                            model.Name = dbObj.name;
                        model.Email = dbObj.email;
                        model.Id = dbObj.id;
                        model.Role = dbObj.role_id;
                        model.Mobile = dbObj.phone_no1;
                        model.EmpCode = dbObj.emp_code;
                        return View(model);
                    }
                }
            }
            return HttpNotFound();
        }



        [HttpGet]
        public ActionResult Details(long? id)
        {
            var prms = (long?[])Session["permission"];
            if (RBL.Helpers.Utility.turnOffAuth() || (prms == null))
                return RedirectToAction("Login", "Account");
            if (Helpers.Utility.turnOffAuth() || !prms.Contains((int)Permission.UserView))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            if (id != null)
            {
                using (var db = new mobisnuu_rblEntities())
                {
                    var dbObj = db.RBL_UserMaster.Where(x => x.id == id).FirstOrDefault();
                    if (dbObj != null)
                    {
                        var model = new UserComman();
                        model.RoleList = db.RBL_RoleName.Select(x => new ListItemRoles { Id = x.id, RoleName = x.role_name }).ToList();
                        var categorylist = db.RBL_Menu.Where(x => x.parent_id == 2).Select(x => new { Id = x.id, Type = x.action_name }).ToList();
                        var allRolePermission = db.RBL_RolePermission.Select(x => new { Id = x.id, RoleId = x.role_id, MenuId = x.menu_id }).ToList();
                        foreach (var item in model.RoleList)
                        {
                            var permissions = allRolePermission.Where(x => x.RoleId == item.Id).Select(x => (long)x.MenuId).ToList();
                            item.permissions = permissions;
                        }
                        var temp = new List<KeyValuePair>();
                        foreach (var item in categorylist)
                        {
                            var tempid = System.Convert.ToString(item.Id);
                            temp.Add(new KeyValuePair { Id = tempid, Type = item.Type });
                        }
                        model.CategoryPermission = temp;
                        var filelist = db.RBL_Menu.Where(x => x.parent_id == 5).Select(x => new { Id = x.id, Type = x.action_name }).ToList();
                        temp = new List<KeyValuePair>();
                        foreach (var item in filelist)
                        {
                            var tempid = System.Convert.ToString(item.Id);
                            temp.Add(new KeyValuePair { Id = tempid, Type = item.Type });
                        }
                        model.FilePermission = temp;
                        var userlist = db.RBL_Menu.Where(x => x.parent_id == 8).Select(x => new { Id = x.id, Type = x.action_name }).ToList();
                        temp = new List<KeyValuePair>();
                        foreach (var item in userlist)
                        {
                            var tempid = System.Convert.ToString(item.Id);
                            temp.Add(new KeyValuePair { Id = tempid, Type = item.Type });
                        }
                        model.UserPermission = temp;
                        model.Name = dbObj.name;
                        model.Email = dbObj.email;
                        model.Id = dbObj.id;
                        model.Role = dbObj.role_id;
                        model.Mobile = dbObj.phone_no1;
                        model.EmpCode = dbObj.emp_code;
                        return View(model);
                    }
                }
            }
            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UserEdit model)
        {
            var prms = (long?[])Session["permission"];
            if (RBL.Helpers.Utility.turnOffAuth() || (prms == null))
                return RedirectToAction("Login", "Account");
            if (Helpers.Utility.turnOffAuth() || !prms.Contains((int)Permission.UserEdit))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
           
            if (model != null)
            {
                using (var db = new mobisnuu_rblEntities())
                {
                    var dbObj = db.RBL_UserMaster.Where(x => x.id == model.Id).FirstOrDefault();
                    if (dbObj != null)
                    {
                        
                        model.RoleList = db.RBL_RoleName.Select(x => new ListItemRoles { Id = x.id, RoleName = x.role_name }).ToList();
                        var categorylist = db.RBL_Menu.Where(x => x.parent_id == 2).Select(x => new { Id = x.id, Type = x.action_name }).ToList();
                        var allRolePermission = db.RBL_RolePermission.Select(x => new { Id = x.id, RoleId = x.role_id, MenuId = x.menu_id }).ToList();
                        foreach (var item in model.RoleList)
                        {
                            var permissions = allRolePermission.Where(x => x.RoleId == item.Id).Select(x => (long)x.MenuId).ToList();
                            item.permissions = permissions;
                        }
                        var temp = new List<KeyValuePair>();
                        foreach (var item in categorylist)
                        {
                            var tempid = System.Convert.ToString(item.Id);
                            temp.Add(new KeyValuePair { Id = tempid, Type = item.Type });
                        }
                        model.CategoryPermission = temp;
                        var filelist = db.RBL_Menu.Where(x => x.parent_id == 5).Select(x => new { Id = x.id, Type = x.action_name }).ToList();
                        temp = new List<KeyValuePair>();
                        foreach (var item in filelist)
                        {
                            var tempid = System.Convert.ToString(item.Id);
                            temp.Add(new KeyValuePair { Id = tempid, Type = item.Type });
                        }
                        model.FilePermission = temp;
                        var userlist = db.RBL_Menu.Where(x => x.parent_id == 8).Select(x => new { Id = x.id, Type = x.action_name }).ToList();
                        temp = new List<KeyValuePair>();
                        foreach (var item in userlist)
                        {
                            var id = System.Convert.ToString(item.Id);
                            temp.Add(new KeyValuePair { Id = id, Type = item.Type });
                        }
                        model.UserPermission = temp;
                        if (ModelState.IsValid)
                        {
                            var sameObj = db.RBL_UserMaster.Where(x=> x.id != model.Id).Where(x => x.email == model.Email || x.phone_no1 == model.Mobile || x.phone_no2 == model.Mobile || x.emp_code == model.EmpCode).FirstOrDefault();
                            var Role = db.RBL_RoleName.Where(x => x.id == model.Role).FirstOrDefault();
                            if (sameObj == null && Role != null && Role.is_active == true)
                            {
                                dbObj.name = model.Name;
                                dbObj.email = model.Email;
                                dbObj.role_id = model.Role;
                                dbObj.phone_no1 = model.Mobile;
                                dbObj.phone_no2 = model.Mobile;
                                dbObj.emp_code = model.EmpCode;
                                dbObj.password = String.IsNullOrEmpty(model.password) ? dbObj.password : RBL.Helpers.Helpers.ComputeHash(model.password, new SHA256CryptoServiceProvider(),dbObj.created_at.ToString());
                                db.SaveChanges();
                                Session["msg"] = "Updated user " + dbObj.name;
                                return RedirectToAction("index");
                            }
                            else
                            {
                                if (model.Email == sameObj.email)
                                    ModelState.AddModelError("", "Same Email exists");
                                if (sameObj.phone_no1 == model.Mobile || sameObj.phone_no2 == model.Mobile)
                                    ModelState.AddModelError("", "Same phone exists");
                                if (sameObj.emp_code == model.EmpCode)
                                    ModelState.AddModelError("", "Same emp code exists");
                                if (Role == null)
                                    ModelState.AddModelError("", "No Role Found for this id");
                                if (Role.is_active == false)
                                    ModelState.AddModelError("", "Role is not active");
                            }

                        }
                        return View(model);
                    }
                }
            }
            return HttpNotFound();
        }


        [HttpGet]
        public ActionResult Delete(long? id, string returnUrl)
        {
            var prms = (long?[])Session["permission"];
            if (RBL.Helpers.Utility.turnOffAuth() || (prms == null))
                return RedirectToAction("Login", "Account");
            if (Helpers.Utility.turnOffAuth() || !prms.Contains((int)Permission.UserEdit))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            
            if (id != null)
            {
                using (var db = new mobisnuu_rblEntities())
                {
                    var dbObj = db.RBL_UserMaster.Where(x => x.id == id).FirstOrDefault();
                    if (dbObj != null)
                    {
                        dbObj.is_active = (bool)dbObj.is_active ? false : true;
                        db.SaveChanges();
                    }
                }
            }
            return RedirectToAction((String.IsNullOrEmpty(returnUrl) ? "Index" : returnUrl));
        }

        
        public ActionResult CommingSoon()
        {
            return View();
        }

        #region roles
        public ActionResult RoleIndex()
        {
            var prms = (long?[])Session["permission"];
            if (RBL.Helpers.Utility.turnOffAuth() || (prms == null))
                return RedirectToAction("Login", "Account");
            if (Helpers.Utility.turnOffAuth() || !(prms.Contains((int)Permission.UserEdit) || prms.Contains((int)Permission.UserAdd) || prms.Contains((int)Permission.UserView)))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            if ((Session["msg"] as string) != null)
            {
                ModelState.AddModelError("", (string)Session["msg"]);
                Session.Remove("msg");
            }
            mobisnuu_rblEntities db = new mobisnuu_rblEntities();
            return View(db.RBL_RoleName.Select(x => new RolesList { Id = x.id, RoleName = x.role_name, Date = x.created_at, isActive = x.is_active }).ToList());
             
        }

        [HttpGet]
        public ActionResult RolesDelete(long? id, string returnUrl)
        {
            var prms = (long?[])Session["permission"];
            if (RBL.Helpers.Utility.turnOffAuth() || (prms == null))
                return RedirectToAction("Login", "Account");
            if (Helpers.Utility.turnOffAuth() || !prms.Contains((int)Permission.UserEdit))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            
            if (id != null)
            {
                using (var db = new mobisnuu_rblEntities())
                {
                    var dbObj = db.RBL_RoleName.Where(x => x.id == id).FirstOrDefault();
                    if (dbObj != null)
                    {
                        dbObj.is_active = (bool)dbObj.is_active ? false : true;
                        db.SaveChanges();
                    }
                }
            }
            return RedirectToAction((String.IsNullOrEmpty(returnUrl) ? "Index" : returnUrl));            
        }

        [HttpGet]
        public ActionResult RolesCreate() {
            var prms = (long?[])Session["permission"];
            if (RBL.Helpers.Utility.turnOffAuth() || (prms == null))
                return RedirectToAction("Login", "Account");
            if (Helpers.Utility.turnOffAuth() || !prms.Contains((int)Permission.UserAdd))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            return View(getRolesCommanModel());
        }
        public RolesComman getRolesCommanModel()
        {
            RolesComman model;
            mobisnuu_rblEntities db = new mobisnuu_rblEntities();
            model = new RolesComman();
            var categorylist = db.RBL_Menu.Where(x => x.parent_id == 2).Select(x => new { Id = x.id, Type = x.action_name }).ToList();
            var temp = new List<KeyValuePair>();
            foreach (var item in categorylist)
            {
                var id = System.Convert.ToString(item.Id);
                temp.Add(new KeyValuePair { Id = id, Type = item.Type });
            }
            model.CategoryPermission = temp;
            var filelist = db.RBL_Menu.Where(x => x.parent_id == 5).Select(x => new { Id = x.id, Type = x.action_name }).ToList();
            temp = new List<KeyValuePair>();
            foreach (var item in filelist)
            {
                var id = System.Convert.ToString(item.Id);
                temp.Add(new KeyValuePair { Id = id, Type = item.Type });
            }
            model.FilePermission = temp;
            var userlist = db.RBL_Menu.Where(x => x.parent_id == 8).Select(x => new { Id = x.id, Type = x.action_name }).ToList();
            temp = new List<KeyValuePair>();
            foreach (var item in userlist)
            {
                var id = System.Convert.ToString(item.Id);
                temp.Add(new KeyValuePair { Id = id, Type = item.Type });
            }
            model.UserPermission = temp;
            return model;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RolesCreate(RolesComman model)
        {
            var prms = (long?[])Session["permission"];
            if (RBL.Helpers.Utility.turnOffAuth() || (prms == null))
                return RedirectToAction("Login", "Account");
            if (Helpers.Utility.turnOffAuth() || !prms.Contains((int)Permission.UserAdd))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            if (ModelState.IsValid)
            {
                try
                {
                    using (mobisnuu_rblEntities db = new mobisnuu_rblEntities())
                    {
                        var sameObj = db.RBL_RoleName.Where(x => x.role_name == model.RoleName).FirstOrDefault();
                        if (sameObj == null)
                        {
                            var roleNamObj = new RBL_RoleName();
                            roleNamObj.guid = Guid.NewGuid().ToString();
                            roleNamObj.is_active = true;
                            roleNamObj.role_name = model.RoleName;
                            roleNamObj.created_by = Convert.ToInt64(Session["uid"]);
                            roleNamObj.updated_by = Convert.ToInt64(Session["uid"]);
                            var currentTime = DateTime.Now;
                            roleNamObj.created_at = currentTime;
                            roleNamObj.updated_at = currentTime;
                            db.RBL_RoleName.Add(roleNamObj);
                            db.SaveChanges();
                            foreach (var item in model.CategoryManagement)
                            {
                                var rolePermissionObj = new RBL_RolePermission();
                                rolePermissionObj.created_by = Convert.ToInt64(Session["uid"]);
                                rolePermissionObj.updated_by = Convert.ToInt64(Session["uid"]);
                                rolePermissionObj.created_at = currentTime;
                                rolePermissionObj.guid = Guid.NewGuid().ToString();
                                rolePermissionObj.updated_at = currentTime;
                                rolePermissionObj.role_id = roleNamObj.id;
                                rolePermissionObj.menu_id = item;
                                db.RBL_RolePermission.Add(rolePermissionObj);
                            }
                            foreach (var item in model.FileManagement)
                            {
                                var rolePermissionObj = new RBL_RolePermission();
                                rolePermissionObj.created_by = Convert.ToInt64(Session["uid"]);
                                rolePermissionObj.updated_by = Convert.ToInt64(Session["uid"]);
                                rolePermissionObj.created_at = currentTime;
                                rolePermissionObj.guid = Guid.NewGuid().ToString();
                                rolePermissionObj.updated_at = currentTime;
                                rolePermissionObj.role_id = roleNamObj.id;
                                rolePermissionObj.menu_id = item;
                                db.RBL_RolePermission.Add(rolePermissionObj);
                            }
                            db.SaveChanges();
                            Session["msg"] = "Record Inserted";
                            return RedirectToAction("RoleIndex", "User");
                        }
                        else
                        {
                            throw new Exception("Role Name already exists");
                        }
                    }

                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("",ex.Message);
                }
               
            }
            return View();
        }

        [HttpGet]
        public ActionResult RolesEdit(long? id)
        {
            var prms = (long?[])Session["permission"];
            if (RBL.Helpers.Utility.turnOffAuth() || (prms == null))
                return RedirectToAction("Login", "Account");
            if (Helpers.Utility.turnOffAuth() || !prms.Contains((int)Permission.UserEdit))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            if (id != null)
            {
                mobisnuu_rblEntities db = new mobisnuu_rblEntities();
                var dbObj = db.RBL_RoleName.Find(id);
                ViewBag.permissions = db.RBL_RolePermission.Where(x => x.role_id == dbObj.id).Select(x => x.menu_id).ToList();
                var model = getRolesCommanModel();
                model.RoleName = dbObj.role_name;
                return View(model);
            }
            else
            {
                return HttpNotFound();
            }
    }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RolesEdit(RolesComman model)
        {
            var prms = (long?[])Session["permission"];
            if (RBL.Helpers.Utility.turnOffAuth() || (prms == null))
                return RedirectToAction("Login", "Account");
            if (Helpers.Utility.turnOffAuth() || !prms.Contains((int)Permission.UserEdit))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            if (ModelState.IsValid)
            {
                try
                {
                    using (var db = new mobisnuu_rblEntities())
                    {
                        var dbRoleObj = db.RBL_RoleName.Find(model.Id);
                        if (dbRoleObj != null)
                        {
                            var sameObj = db.RBL_RoleName.Where(x => x.id != model.Id && x.role_name == model.RoleName).FirstOrDefault();
                            if (sameObj == null)
                            {
                                dbRoleObj.role_name = model.RoleName;
                                var dbList = db.RBL_RolePermission.Where(x => x.role_id == model.Id).Select(x => (int)x.menu_id).ToList();
                                var editedList = model.CategoryManagement;
                                var added = editedList.Except(dbList).ToList();
                                var removed = dbList.Except(editedList).ToList();
                                foreach (var item in removed)
                                {
                                    var dbObj = db.RBL_RolePermission.Where(x => x.role_id == model.Id).Where(x => x.menu_id == item).SingleOrDefault();
                                    if (dbObj != null)
                                    {
                                        db.Entry(dbObj).State = System.Data.Entity.EntityState.Deleted;
                                    }
                                }
                                foreach (var item in added)
                                {
                                    var dbObj = db.RBL_RolePermission.Where(x => x.role_id == model.Id).Where(x => x.menu_id == item).SingleOrDefault();
                                    if (dbObj == null)
                                    {
                                        var obj = new RBL_RolePermission();
                                        var createrId = System.Convert.ToInt64(Session["uid"]);
                                        obj.created_by = createrId;
                                        obj.updated_by = createrId;
                                        var currentTime = DateTime.Now;
                                        obj.created_at = currentTime;
                                        obj.updated_at = currentTime;
                                        obj.guid = Guid.NewGuid().ToString();
                                        obj.role_id = model.Id;
                                        obj.menu_id = item;
                                        db.RBL_RolePermission.Add(obj);
                                    }
                                }
                                db.SaveChanges();
                                Session["msg"] =String.Format("Updated Role {0}", dbRoleObj.role_name );
                            }
                            else
                            {
                                ModelState.AddModelError("", "Role with same Name exists");
                                return View(model);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Session["msg"] = ex.Message;
                }
                return RedirectToAction("RoleIndex");

            }
            return View();
            
        }


        public ActionResult RolesDetails(long? id)
        {
            var prms = (long?[])Session["permission"];
            if (RBL.Helpers.Utility.turnOffAuth() || (prms == null))
                return RedirectToAction("Login", "Account");
            if (Helpers.Utility.turnOffAuth() || !prms.Contains((int)Permission.UserView))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            
            if (id != null)
            {
                mobisnuu_rblEntities db = new mobisnuu_rblEntities();
                var dbObj = db.RBL_RoleName.Find(id);
                ViewBag.permissions = db.RBL_RolePermission.Where(x => x.role_id == dbObj.id).Select(x => x.menu_id).ToList();
                var model = getRolesCommanModel();
                model.RoleName = dbObj.role_name;
                return View(model);
            }
            else
            {
                return HttpNotFound();
            }
        }
        #endregion
    }
}