﻿using ImageProcessor;
using RBL.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using RBL.Models;
using System.Net;
using RBL.Enums;
using RBL.Helpers;

namespace RBL.Controllers
{
    [Authorize]
    [NoCache]
    public class CategoryController : BaseController
    {
        #region category crud
        // GET: Category
        public ActionResult Index(string msg)
        {
            var prms = (long?[])Session["permission"];
            if (RBL.Helpers.Utility.turnOffAuth() || (prms == null))
            {
                return RedirectToAction("Login","Account");
            }
            if (Helpers.Utility.turnOffAuth() || !(prms.Contains((int)Permission.CategoryView) || prms.Contains((int)Permission.CategoryEdit) || prms.Contains((int)Permission.CategoryAdd)))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            msg = (string)Session["msg"];
            if (!String.IsNullOrEmpty(msg))
            {
                RouteData.Values.Remove("msg");
                ModelState.AddModelError("",msg);
                Session.Remove("msg");
            }
            ViewBag.list = new List<SelectListItem> { new SelectListItem { Text = "Activate", Value = "0" }, new SelectListItem { Text = "Deactivate", Value = "1" } };
            var viewmodellist = CategoryModel.getParentCategory();
            return View(viewmodellist);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ParentCategoryAddViewModel model)
        {
            var prms = (long?[])Session["permission"];
            if (RBL.Helpers.Utility.turnOffAuth() || (prms == null))
            {
                return RedirectToAction("Login", "Account");
            }
            if (Helpers.Utility.turnOffAuth() || !prms.Contains((int)Permission.CategoryAdd))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            if (ModelState.IsValid)
            {
                var catManObj = new CategoryModel(model);
                var message = "";
                if (catManObj.SaveCategory())
                {
                    ViewBag.message = "New Record created";
                    Session["msg"] = "New Record created";
                    return RedirectToAction("Index", "Category");
                }
                else
                {
                    message = "Couldnt insert";
                    return RedirectToAction("Index", "Category", new { msg = message });
                }
            }
            return View();
        }
        
        public ActionResult Edit(long? id)
        {
            var prms = (long?[])Session["permission"];
            if (RBL.Helpers.Utility.turnOffAuth() || (prms == null))
            {
                return RedirectToAction("Login", "Account");
            }
            if (Helpers.Utility.turnOffAuth() || !prms.Contains((int)Permission.CategoryEdit))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var db = new mobisnuu_rblEntities();
            RBL_Category rBL_Category = db.RBL_Category.Find(id);
            if (rBL_Category == null)
            {
                return HttpNotFound();
            }
            else
            {
                var viewObj = new ParentCategoryEditViewModel();
                viewObj.Id = rBL_Category.id;
                viewObj.Name = rBL_Category.name;
                return View(viewObj);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ParentCategoryEditViewModel model)
        {
            var prms = (long?[])Session["permission"];
            if (RBL.Helpers.Utility.turnOffAuth() || (prms == null))
                return RedirectToAction("Login", "Account");
            if (Helpers.Utility.turnOffAuth() || !prms.Contains((int)Permission.CategoryEdit))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            var db = new mobisnuu_rblEntities();
            var dbObj = db.RBL_Category.Where(x => x.id == model.Id).FirstOrDefault();
            if (dbObj != null)
            {
                var sameObj = db.RBL_Category.Where(x => x.name == model.Name).FirstOrDefault();
                if (sameObj == null)
                {
                    dbObj.name = model.Name;
                }
                else
                {
                    ModelState.AddModelError("", "name already exists");
                    return View();
                }
                db.SaveChanges();
                Session["msg"] = "Updated Record";
                return RedirectToAction("Index", "Category");
            }
            else
            {
                return HttpNotFound();
            }
        }
        
        public ActionResult Create()
        {
            var prms = (long?[])Session["permission"];
            if (RBL.Helpers.Utility.turnOffAuth() || (prms == null))
                return RedirectToAction("Login", "Account");
            if (Helpers.Utility.turnOffAuth() || !prms.Contains((int)Permission.CategoryAdd))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            ModelState.Clear();
            return View();
        }

        public ActionResult Details(long? id)
        {
            var prms = (long?[])Session["permission"];
            if (RBL.Helpers.Utility.turnOffAuth() || (prms == null))
                return RedirectToAction("Login", "Account");
            if (Helpers.Utility.turnOffAuth() || !prms.Contains((int)Permission.CategoryView))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var db = new mobisnuu_rblEntities();
            RBL_Category rBL_Category = db.RBL_Category.Find(id);
            if (rBL_Category == null)
            {
                return HttpNotFound();
            }
            else
            {
                var viewObj = new ParentCategoryEditViewModel();
                viewObj.Id = rBL_Category.id;
                viewObj.Name = rBL_Category.name;
                return View(viewObj);
            }
        }


        #endregion

        #region common
        //not working closed
        public ActionResult BulkAction(IEnumerable<string> BulkId, string BulkOption, string returnUrl)
        {
            if (BulkId != null)
            {
                using (var db = new mobisnuu_rblEntities())
                {
                    foreach (var item in BulkId)
                    {
                        if (BulkOption == "0")
                        {
                            var dbObj = db.RBL_Category.Find(Convert.ToInt64(item));
                            if (dbObj != null)
                            {
                                dbObj.is_active = true;
                            }
                        }
                        else if (BulkOption == "1")
                        {
                            var dbObj = db.RBL_Category.Find(Convert.ToInt64(item));
                            if (dbObj != null)
                            {
                                dbObj.is_active = false;
                            }
                        }
                    }
                    db.SaveChanges();
                }
            }
            return RedirectToAction((String.IsNullOrEmpty(returnUrl) ? "Index" : returnUrl));

        }


        public ActionResult Delete(long? id,string returnUrl) {
            var prms = (long?[])Session["permission"];
            if (RBL.Helpers.Utility.turnOffAuth() || (prms == null))
                return RedirectToAction("Login", "Account");
            if (Helpers.Utility.turnOffAuth() || !prms.Contains((int)Permission.CategoryEdit))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            if (id != null)
            {
                using (var db = new mobisnuu_rblEntities())
                {
                    var dbObj = db.RBL_Category.Where(x => x.id == id).FirstOrDefault();
                    if (dbObj != null)
                    {
                        dbObj.is_active = dbObj.is_active ? false : true;
                    }
                    db.SaveChanges();
                }
            }
            return RedirectToAction((String.IsNullOrEmpty(returnUrl) ? "Index" : returnUrl));

        }
        #endregion


        #region  sub Category
        [HttpGet]
        public ActionResult SubIndex(string msg)
        {
            var prms = (long?[])Session["permission"];
            if (RBL.Helpers.Utility.turnOffAuth() || (prms == null))
                return RedirectToAction("Login", "Account");
            if (Helpers.Utility.turnOffAuth() || !(prms.Contains((int)Permission.CategoryView) || prms.Contains((int)Permission.CategoryEdit) || prms.Contains((int)Permission.CategoryAdd)))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            msg = (string)Session["msg"];
            if (!String.IsNullOrEmpty(msg))
            {
                RouteData.Values.Remove("msg");
                ModelState.AddModelError("", msg);
                Session.Remove("msg");
            }
            ViewBag.list = new List<SelectListItem> { new SelectListItem { Text = "Activate", Value = "0" }, new SelectListItem { Text = "Deactivate", Value = "1" } };
            var viewmodellist = CategoryModel.getChildCategory();
            return View(viewmodellist);
        }
        
        public ActionResult SubEdit(long? id)
        {
            var prms = (long?[])Session["permission"];
            if (RBL.Helpers.Utility.turnOffAuth() || (prms == null))
                return RedirectToAction("Login", "Account");
            if (Helpers.Utility.turnOffAuth() || !prms.Contains((int)Permission.CategoryEdit))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            if (id != null)
            {
                var db = new mobisnuu_rblEntities();
                ChildCategoryViewModel Obj = db.RBL_Category.Join(db.RBL_Category, m => m.id, j => j.parent_id, (r, j) => new ChildCategoryViewModel { ParentName = r.name, SubName = j.name, Image = j.image, Discription = j.description, Date = j.created_at, Id = j.id, parent_id = r.id }).Where(x => x.Id == id).FirstOrDefault();
                if (Obj == null)
                {
                    return HttpNotFound();
                }
                else
                {
                    List<SelectListItem> selectList = CategoryModel.getParentCategory().Select(x => new SelectListItem { Text = x.Name, Value = Convert.ToString(x.Id) }).ToList();
                    var viewModal = new ChildCategoryEditViewModel();
                    viewModal.ParentCategoryList = selectList;
                    viewModal.ParentCategory = Obj.parent_id;
                    viewModal.selectedImage = Obj.Image;
                    viewModal.SubCategory = Obj.SubName;
                    viewModal.Description = Obj.Discription;
                    viewModal.Id = Obj.Id;
                    return View(viewModal);
                }

            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SubEdit(ChildCategoryEditViewModel model)
        {
            var prms = (long?[])Session["permission"];
            if (RBL.Helpers.Utility.turnOffAuth() || (prms == null))
                return RedirectToAction("Login", "Account");
            if (Helpers.Utility.turnOffAuth() || !prms.Contains((int)Permission.CategoryEdit))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            
            List<SelectListItem> selectList = CategoryModel.getParentCategory().Select(x => new SelectListItem { Text = x.Name, Value = Convert.ToString(x.Id) }).ToList();
            model.ParentCategoryList = selectList;
            if (ModelState.IsValid)
            {
                try
                {
                    using (var db = new mobisnuu_rblEntities())
                    {
                        var dbObj = db.RBL_Category.Where(x => x.id == model.Id).FirstOrDefault();
                        if (dbObj != null)
                        {
                            var sameObj = db.RBL_Category.Where(x=>x.id != model.Id).Where(x => x.name == model.SubCategory).Where(x => x.parent_id == model.ParentCategory).FirstOrDefault();
                            if (sameObj == null)
                            {
                                var savedFileName = model.selectedImage;
                                if (model.image != null)
                                {
                                    savedFileName = SaveFile(model.image);
                                    try
                                    {
                                        System.IO.File.Delete(MediaModel.uploadDir + dbObj.image);
                                    }
                                    catch (Exception)
                                    {
                                        System.Diagnostics.Debug.WriteLine("couldt not find file");
                                    }
                                    
                                }
                                dbObj.name = model.SubCategory;
                                dbObj.description = model.Description;
                                dbObj.updated_at = DateTime.Now;
                                dbObj.updated_by = Convert.ToInt64(Session["uid"]);
                                dbObj.image = savedFileName;
                                dbObj.parent_id = model.ParentCategory;
                            }
                            else
                            {
                                ModelState.AddModelError("", "name already exists in the selected category");
                                return View(model);
                            }
                            db.SaveChanges();
                            Session["msg"] = "Updated Record";
                            return RedirectToAction("SubIndex", "Category");
                        }
                        else
                        {
                            return new HttpStatusCodeResult(HttpStatusCode.NotFound);
                        }
                    }

                }
                catch (Exception)
                {
                    ModelState.AddModelError("", "Exception occured");
                    return View(model);
                }
            }
            else
            {
                return View(model);
            }
           
        }

        public ActionResult SubCreate(long? id)
        {
            var prms = (long?[])Session["permission"];
            if (RBL.Helpers.Utility.turnOffAuth() || (prms == null))
                return RedirectToAction("Login", "Account");
            if (Helpers.Utility.turnOffAuth() || !prms.Contains((int)Permission.CategoryAdd))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
                        
            List<SelectListItem> selectList =  CategoryModel.getParentCategory().Select(x => new SelectListItem { Text = x.Name , Value = Convert.ToString(x.Id)}).ToList();
            var viewModal = new ChildCategoryAddViewModel();
            viewModal.ParentCategoryList = selectList;
            viewModal.ParentCategory = null;
            if (id !=null)
            {
                viewModal.ParentCategory = id;
            }
            return View(viewModal);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SubCreate(ChildCategoryAddViewModel model)
        {
            var prms = (long?[])Session["permission"];
            if (RBL.Helpers.Utility.turnOffAuth() || (prms == null))
                return RedirectToAction("Login", "Account");
            if (Helpers.Utility.turnOffAuth() || !prms.Contains((int)Permission.CategoryAdd))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            List<SelectListItem> selectList = CategoryModel.getParentCategory().Select(x => new SelectListItem { Text = x.Name, Value = Convert.ToString(x.Id) }).ToList();
            var viewModal = new ChildCategoryAddViewModel();
            viewModal.ParentCategoryList = selectList;
            if (ModelState.IsValid)
            {
                
                try
                {
                    using (var db = new mobisnuu_rblEntities())
                    {
                        var catObj = db.RBL_Category.Where(x => x.name.ToLower() == model.SubCategory.ToLower()).Where(x=>x.parent_id == model.ParentCategory).FirstOrDefault();
                        if (catObj == null)
                        {
                            var savedFileName = SaveFile(model.image);
                            if (savedFileName == null)
                            {
                                ModelState.AddModelError("", "couldnt save Image");
                            }
                            catObj = new RBL_Category();
                            catObj.name = model.SubCategory;
                            catObj.created_by = Convert.ToInt64(Session["uid"]);
                            catObj.updated_by = Convert.ToInt64(Session["uid"]);
                            DateTime? currentTime = DateTime.Now;
                            catObj.created_at = currentTime;
                            catObj.updated_at = currentTime;
                            catObj.image = savedFileName;
                            catObj.guid = Guid.NewGuid().ToString();
                            catObj.parent_id = model.ParentCategory;
                            catObj.description = model.Description;
                            db.RBL_Category.Add(catObj);
                            db.SaveChanges();
                            Session["msg"] = "New Record created";
                            return RedirectToAction("SubIndex", "Category");
                        }
                        else
                        {
                            ModelState.AddModelError("", "name already exists in same category");
                            return View(viewModal);
                        }
                    }
                    
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", "Exception occured");
                    return View(viewModal);
                }
            }
            return View(viewModal);
        }

        public ActionResult SubDetails(long? id)
        {
            var prms = (long?[])Session["permission"];
            if (RBL.Helpers.Utility.turnOffAuth() || (prms == null))
                return RedirectToAction("Login", "Account");
            if (Helpers.Utility.turnOffAuth() || !prms.Contains((int)Permission.CategoryView))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            
            if (id != null)
            {
                var db = new mobisnuu_rblEntities();
                ChildCategoryViewModel Obj = db.RBL_Category.Join(db.RBL_Category, m => m.id, j => j.parent_id, (r, j) => new ChildCategoryViewModel { ParentName = r.name, SubName = j.name, Image = j.image, Discription = j.description, Date = j.created_at, Id = j.id, parent_id = r.id }).Where(x => x.Id == id).FirstOrDefault();
                if (Obj == null)
                {
                    return HttpNotFound();
                }
                else
                {
                    List<SelectListItem> selectList = CategoryModel.getParentCategory().Select(x => new SelectListItem { Text = x.Name, Value = Convert.ToString(x.Id) }).ToList();
                    var viewModal = new ChildCategoryEditViewModel();
                    viewModal.ParentCategoryList = selectList;
                    viewModal.ParentCategory = Obj.parent_id;
                    viewModal.selectedImage = Obj.Image;
                    viewModal.SubCategory = Obj.SubName;
                    viewModal.Description = Obj.Discription;
                    viewModal.Id = Obj.Id;
                    return View(viewModal);
                }

            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        private String SaveFile(HttpPostedFileBase file)
        {
            try
            {
                if (file.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    var savedFilename = Guid.NewGuid().ToString() + fileName;
                    var path = Path.Combine(MediaModel.uploadDir, savedFilename);
                    file.SaveAs(path);
                    return savedFilename;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        #endregion
    }
}