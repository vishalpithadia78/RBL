﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RBL.Models;
using System.Dynamic;
using System.Net;
using RBL.Enums;
using PagedList;

namespace RBL.Controllers
{
    [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
    [Authorize]
    public class MediaController : BaseController
    {
        mobisnuu_rblEntities db = new mobisnuu_rblEntities();
        // GET: Media
        [HttpGet]
        public ActionResult Index(int? Page)
        {
            var prms = (long?[])Session["permission"];
            if (RBL.Helpers.Utility.turnOffAuth() || (prms == null))
                return RedirectToAction("Login", "Account");
            if (Helpers.Utility.turnOffAuth() || !(prms.Contains((int)Permission.FileView) || prms.Contains((int)Permission.FileEdit) || prms.Contains((int)Permission.FileAdd)))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            if (!String.IsNullOrEmpty(Session["msg"] as string))
            {
                ModelState.AddModelError("",(string) Session["msg"]);
                Session.Remove("msg");
            }
            using (var db = new mobisnuu_rblEntities())
            {
                Page = Page ?? 1;
                ViewBag.Index = ((Page - 1 ) * 15);
                var list = db.RBL_MediaMaster.Where(x => x.parent_id == 0).Join(db.view_category_relationship,m=>m.id,v=>v.media_master_id,(m,v)=> new IndexMediaViewModel { Category = v.parent_name, Date = m.created_at, Id = m.id, Image = (String.IsNullOrEmpty(m.thumbnail)?m.filename:m.thumbnail),Title = m.title, isActive = m.is_active, Type = (m.is_project ? "Project": (String.IsNullOrEmpty(m.extension)?"video":m.extension))  }).OrderBy(x=>x.Id).ToList().ToPagedList((int)Page,15);
                return View(list);
            }            
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditMediaViewModel model  )
        {
            var prms = (long?[])Session["permission"];
            if (RBL.Helpers.Utility.turnOffAuth() || (prms == null))
                return RedirectToAction("Login", "Account");
            if (Helpers.Utility.turnOffAuth() || !prms.Contains((int)Permission.FileEdit))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            
            if (model != null)
            {
                using (var db = new mobisnuu_rblEntities())
                {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        var dbObj = db.RBL_MediaMaster.Where(x => x.id == model.Id).FirstOrDefault();
                        if (dbObj != null)
                        {
                            model.Project = dbObj.is_project ? "1" : "0";
                            model.CategoryList = new SelectList(getAllCategory(true), "id", "name");
                            model.SubCategoryList = getAllCategory(false).Select(x => new SubCategory { Id = x.parent_id, Value = x.id, Text = x.name }).ToList();
                            model.PdfTypeList = new List<SelectListItem> { new SelectListItem { Text = "Leaflets", Value = "4" }, new SelectListItem { Text = "Brochures", Value = "6" }, new SelectListItem { Text = "Forms", Value = "5" } };
                            // is project or not
                            model.ProjectOption = new List<KeyValuePair>{
                                new KeyValuePair { Id = "1", Type = "Yes" },
                                new KeyValuePair { Id = "0", Type = "No" }
                                };
                            model.SelectTypeNonProjectOption = getAllFileTypes();
                            var allFiles = getRelatedFiles(model.Id);

                            var catId = Convert.ToInt64(model.Category);
                            model.CategoryName = db.RBL_Category.Where(x => x.id == catId).Select(x => x.name).FirstOrDefault();
                            var subCatId = Convert.ToInt64(model.SubCategory);
                            model.SubCategoryName = db.RBL_Category.Where(x => x.id == subCatId).Select(x => x.name).FirstOrDefault();

                            model.UploadedImagelist = allFiles.Where(x => x.filetype_parent_id == 1).Select(x => new UploadedContent { Id = x.id, Name = x.filename }).ToList();
                            model.UploadedVideoThumblist = allFiles.Where(x => x.filetype_parent_id == 2).Select(x => new UploadedContent { Id = x.id, Name = x.filename, thumb = x.thumbnail }).ToList();
                            model.UploadedPdflist = allFiles.Where(x => x.filetype_parent_id == 3).Select(x => new UploadedContent { Id = x.id, Name = x.filename, thumb = x.thumbnail, Type = x.filetype_child_id }).ToList();

                            var sameDbObj = db.RBL_MediaMaster.Where(x => x.title == model.Title && x.id != model.Id && x.parent_id != model.Id).FirstOrDefault();
                            if (!ModelState.IsValid)
                            {
                                    throw new Exception("");
                            }
                            if (sameDbObj == null)
                            {
                                if (model.UploadedVideo == null)
                                {
                                    model.UploadedVideo = new List<long>();
                                }
                                if (model.UploadedPdf == null)
                                {
                                    model.UploadedPdf = new List<long>();
                                }
                                if (model.UploadedImage == null)
                                {
                                    model.UploadedImage = new List<long>();
                                }
                                model.SelectTypeNonProject = dbObj.filetype_parent_id.ToString();
                                MediaModel mm = new MediaModel(model, db,Convert.ToInt64(Session["uid"]));
                                mm.updateMedia();
                                Session["msg"] = "Updated entry successfully";
                                    
                            }
                            else
                            {
                                throw new Exception("Same Title exists");
                            }
                        }
                        db.SaveChanges();
                        transaction.Commit();
                        return RedirectToAction("Index");
                    }
                    catch (Exception ex)
                        {
                        transaction.Rollback();
                        ModelState.AddModelError("", ex.Message);
                        return View(model);
                    }
                    }
                    }
            }
            return HttpNotFound();

        }

        public ActionResult Details(long? id)
        {
            var prms = (long?[])Session["permission"];
            if (RBL.Helpers.Utility.turnOffAuth() || (prms == null))
                return RedirectToAction("Login", "Account");
            if (Helpers.Utility.turnOffAuth() || !prms.Contains((int)Permission.FileView))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            
            if (id != null)
            {
                RBL_MediaMaster dbObj;
                using (var db = new mobisnuu_rblEntities())
                {
                    dbObj = db.RBL_MediaMaster.Where(x => x.id == id && x.is_active == false).FirstOrDefault();
                    if (dbObj != null)
                    {
                        var model = new EditMediaViewModel();
                        model.CategoryList = new SelectList(getAllCategory(true), "id", "name");
                        model.SubCategoryList = getAllCategory(false).Select(x => new SubCategory { Id = x.parent_id, Value = x.id, Text = x.name }).ToList();
                        model.PdfTypeList = new List<SelectListItem> { new SelectListItem { Text = "Leaflets", Value = "4" }, new SelectListItem { Text = "Brochures", Value = "6" }, new SelectListItem { Text = "Forms", Value = "5" } };
                        // is project or not
                        model.ProjectOption = new List<KeyValuePair>{
                        new KeyValuePair { Id = "1", Type = "Yes" },
                        new KeyValuePair { Id = "0", Type = "No" }
                        };

                        var categoryRelationship = db.view_category_relationship.Where(x => x.media_master_id == dbObj.id).FirstOrDefault();
                        model.Category = categoryRelationship.category_id.ToString();
                        model.SubCategory = categoryRelationship.child_id.ToString();
                        model.CategoryName = categoryRelationship.parent_name;
                        model.SubCategoryName = categoryRelationship.child_name;

                        var allFiles = getRelatedFiles(id);

                        model.UploadedImagelist = allFiles.Where(x => x.filetype_parent_id == 1).Select(x => new UploadedContent { Id = x.id, Name = x.filename }).ToList();
                        model.UploadedVideoThumblist = allFiles.Where(x => x.filetype_parent_id == 2).Select(x => new UploadedContent { Id = x.id, Name = x.filename, thumb = x.thumbnail }).ToList();
                        model.UploadedPdflist = allFiles.Where(x => x.filetype_parent_id == 3).Select(x => new UploadedContent { Id = x.id, Name = x.filename, thumb = x.thumbnail, Type = x.filetype_child_id }).ToList();

                        model.SelectTypeNonProjectOption = getAllFileTypes();
                        // all available types
                        model.Title = dbObj.title;
                        model.Description = dbObj.description;
                        var tempHolder = db.view_keyword_relationship.Where(x => x.media_master_id == dbObj.id).Select(x => x.name).ToArray();
                        model.Keywords = String.Join(",", tempHolder);
                        tempHolder = db.view_language_relationship.Where(x => x.media_master_id == dbObj.id).Select(x => x.name).ToArray();
                        model.Language = String.Join(",", tempHolder);
                        tempHolder = db.view_location_relationship.Where(x => x.media_master_id == dbObj.id).Select(x => x.name).ToArray();
                        model.Location = String.Join(",", tempHolder);
                        model.Project = dbObj.is_project ? "1" : "0";
                        return View(model);
                    }
                }
            }
            return HttpNotFound();
           
        }

        [HttpGet]
        public ActionResult Edit(long? id)
        {
            var prms = (long?[])Session["permission"];
            if (RBL.Helpers.Utility.turnOffAuth() || (prms == null))
                return RedirectToAction("Login", "Account");
            if (Helpers.Utility.turnOffAuth() || !prms.Contains((int)Permission.FileEdit))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            
            if (id != null)
            {
                RBL_MediaMaster dbObj;
                using (var db = new mobisnuu_rblEntities())
                {
                    dbObj = db.RBL_MediaMaster.Where(x => x.id == id && x.is_active == false).FirstOrDefault();
                    if (dbObj != null)
                    {
                        var model = new EditMediaViewModel();
                        model.Id = dbObj.id;
                        model.CategoryList = new SelectList(getAllCategory(true), "id", "name");
                        model.SubCategoryList = getAllCategory(false).Select(x => new SubCategory { Id = x.parent_id, Value = x.id, Text = x.name }).ToList();
                        model.PdfTypeList = new List<SelectListItem> { new SelectListItem { Text = "Leaflets", Value = "4" }, new SelectListItem { Text = "Brochures", Value = "6" }, new SelectListItem { Text = "Forms", Value = "5" } };
                        // is project or not
                        model.ProjectOption = new List<KeyValuePair>{
                        new KeyValuePair { Id = "1", Type = "Yes" },
                        new KeyValuePair { Id = "0", Type = "No" }
                        };

                        var categoryRelationship = db.view_category_relationship.Where(x => x.media_master_id == dbObj.id).FirstOrDefault();
                        model.Category = categoryRelationship.category_id.ToString();
                        model.SubCategory = categoryRelationship.child_id.ToString();
                        model.CategoryName = categoryRelationship.parent_name;
                        model.SubCategoryName = categoryRelationship.child_name;

                        var allFiles = getRelatedFiles(id);

                        model.UploadedImagelist = allFiles.Where(x => x.filetype_parent_id == 1).Select(x => new UploadedContent { Id = x.id,Name = x.filename }).ToList();
                        model.UploadedVideoThumblist = allFiles.Where(x => x.filetype_parent_id == 2).Select(x => new UploadedContent { Id = x.id, Name = x.filename,thumb = x.thumbnail }).ToList();
                        model.UploadedPdflist = allFiles.Where(x => x.filetype_parent_id == 3).Select(x => new UploadedContent { Id = x.id, Name = x.filename,thumb = x.thumbnail, Type = x.filetype_child_id }).ToList();

                        model.SelectTypeNonProjectOption = getAllFileTypes();
                        // all available types
                        model.Title = dbObj.title;
                        model.Description = dbObj.description;
                        var tempHolder = db.view_keyword_relationship.Where(x => x.media_master_id == dbObj.id).Select(x => x.name).ToArray();
                        model.Keywords = String.Join(",", tempHolder);
                        tempHolder = db.view_language_relationship.Where(x => x.media_master_id == dbObj.id).Select(x => x.name).ToArray();
                        model.Language = String.Join(",", tempHolder);
                        tempHolder = db.view_location_relationship.Where(x => x.media_master_id == dbObj.id).Select(x => x.name).ToArray();
                        model.Location = String.Join(",", tempHolder);
                        model.Project = dbObj.is_project ? "1" : "0";
                        return View(model);
                    }
                }
            }
            return HttpNotFound();
            
        }

        [NonAction]
        private List<RBL_MediaMaster> getRelatedFiles(long? id)
        {
            List<dynamic> listOfFiles;
            using (var db = new mobisnuu_rblEntities())
            {
                var list = db.RBL_MediaMaster.Where(x => x.id == id || x.parent_id == id).ToList();
                if (list != null)
                {
                    var firstOfList = list.ElementAt(0);
                    if (firstOfList.is_project && firstOfList.parent_id == 0)
                    {
                        listOfFiles = new List<dynamic>();
                        list.RemoveAt(0);
                        return list;
                    }
                    else if (!list.ElementAt(0).is_project)
                    {
                        return list;
                    }
                    else
                    {
                        System.Diagnostics.Debug.WriteLine("Couldnt not find first object");
                        throw new Exception("Couldnt Find first Object");
                    }
                }
                else
                {
                    throw new Exception("Couldnt find any object from database");
                }
            }
        }

        [HttpGet]
        public ActionResult Delete(long? id, string returnUrl)
        {
            var prms = (long?[])Session["permission"];
            if (RBL.Helpers.Utility.turnOffAuth() || (prms == null))
                return RedirectToAction("Login", "Account");
            if (Helpers.Utility.turnOffAuth() || !prms.Contains((int)Permission.FileEdit))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            
            if (id != null)
            {
                using (var db = new mobisnuu_rblEntities())
                {
                    var dbObj = db.RBL_MediaMaster.Where(x => x.id == id).FirstOrDefault();
                    if (dbObj != null)
                    {
                        dbObj.is_active = !dbObj.is_active ? true : false;
                    }
                    db.SaveChanges();
                }
            }
            return RedirectToAction((String.IsNullOrEmpty(returnUrl) ? "Index" : returnUrl));
            
        }

        [HttpGet]
        public ActionResult Create()
        {
            var prms = (long?[])Session["permission"];
            if (RBL.Helpers.Utility.turnOffAuth() || (prms == null))
                return RedirectToAction("Login", "Account");
            if (Helpers.Utility.turnOffAuth() || !prms.Contains((int)Permission.FileAdd))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            
            return View(getEmptyModel());
        }

        private InsertMediaCommonViewModel getEmptyModel(InsertMediaCommonViewModel model = null)
        {

            model = model ?? new InsertMediaCommonViewModel();
            var returnModel = new InsertMediaCommonViewModel();
            returnModel.Category = model.Category;
            returnModel.Title = model.Title;
            returnModel.Description = model.Description;
            returnModel.SubCategory = model.SubCategory;
            returnModel.Keywords = model.Keywords;
            returnModel.Language = model.Language;
            returnModel.Location = model.Location;
            returnModel.CategoryList = new SelectList(getAllCategory(true), "id", "name");
            returnModel.SubCategoryList = getAllCategory(false).Select(x=> new SubCategory { Id = x.parent_id, Value = x.id, Text = x.name}).ToList();
            returnModel.PdfTypeList = new List<SelectListItem> { new SelectListItem { Text = "Leaflets",Value = "4" },new SelectListItem { Text = "Brochures", Value = "6" },new SelectListItem { Text = "Forms", Value = "5" } };
            // is project or not
            returnModel.ProjectOption = new List<KeyValuePair>{
                    new KeyValuePair { Id = "1", Type = "Yes" },
                    new KeyValuePair { Id = "0", Type = "No" }
                };
            // all available types
            returnModel.SelectTypeNonProjectOption = getAllFileTypes();
            return returnModel;
        }

        public IEnumerable<KeyValuePair> getAllFileTypes() {
            return new List<KeyValuePair>{
                    new KeyValuePair { Id = "1", Type = "Image" },
                    new KeyValuePair { Id = "2", Type = "Video" },
                    new KeyValuePair { Id = "3", Type = "Pdf" }
                };
        }

        [NonAction]
        public IEnumerable<RBL_Category> getAllCategory(bool parent) {
            try
            {
                List<RBL_Category> categories;
                if (parent)
                {
                    categories = db.RBL_Category.Where(x=>x.delete_status==false && x.is_active == true && x.parent_id == 0).ToList();
                }
                else {
                    categories = db.RBL_Category.Where(x => x.delete_status == false && x.is_active == true && x.parent_id != 0).ToList();
                }
                return categories;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(InsertMediaCommonViewModel model)
        {
            var prms = (long?[])Session["permission"];
            if (RBL.Helpers.Utility.turnOffAuth() || (prms == null))
                return RedirectToAction("Login", "Account");
            if (Helpers.Utility.turnOffAuth() || !prms.Contains((int)Permission.FileAdd))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            
            if (ModelState.IsValid) {
                try
                {
                    if (model.imagelist == null || model.imagelist.ElementAt(0) == null)
                    {
                        if (model.videoThumblist == null || model.videoThumblist.ElementAt(0) == null)
                        {
                            if (model.pdflist == null || model.pdflist.ElementAt(0) == null)
                            {
                               ModelState.AddModelError("", "No file selected to upload");
                               return View(getEmptyModel(model));
                            }
                        }
                    }
                    using (var db = new mobisnuu_rblEntities())
                    {
                        var sameObj = db.RBL_MediaMaster.Where(x => x.title == model.Title).FirstOrDefault();
                        if (sameObj != null)
                        {
                            throw new Exception("Same Title exists");
                        }
                    }
                    MediaModel mm = new MediaModel(model, Convert.ToInt64(Session["uid"]));
                    if (model.IsProject())
                    {
                        if (mm.SaveProject())
                        {
                            Session["msg"] = "New record has been created";
                            return RedirectToAction("Index");
                        }
                        else
                        {
                            ModelState.AddModelError("", "something went wrong");
                        }
                    }
                    else
                    {
                        if (mm.SaveNonProject())
                        {
                            Session["msg"] = "New record has been created";
                            return RedirectToAction("Index");
                        }
                        else
                        {
                            ModelState.AddModelError("", "something went wrong");
                        }
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("",ex.Message);
                }
            }
            return View(getEmptyModel(model));
            
        }


    }
}