﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RBL.Models;

namespace RBL.Controllers
{
    public class RBL_CategoryController : Controller
    {
        private mobisnuu_rblEntities db = new mobisnuu_rblEntities();

        // GET: RBL_Category
        public ActionResult Index()
        {
            return View(db.RBL_Category.ToList());
        }

        // GET: RBL_Category/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RBL_Category rBL_Category = db.RBL_Category.Find(id);
            if (rBL_Category == null)
            {
                return HttpNotFound();
            }
            return View(rBL_Category);
        }

        // GET: RBL_Category/Create
        public ActionResult Create()
        {
            ModelState.Clear();
            return View();
        }

        // POST: RBL_Category/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,guid,name,description,image,created_by,created_at,updated_by,updated_at,deleted_by,deleted_at,delete_status")] RBL_Category rBL_Category)
        {
            if (ModelState.IsValid)
            {
                db.RBL_Category.Add(rBL_Category);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(rBL_Category);
        }

        // GET: RBL_Category/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RBL_Category rBL_Category = db.RBL_Category.Find(id);
            if (rBL_Category == null)
            {
                return HttpNotFound();
            }
            return View(rBL_Category);
        }

        // POST: RBL_Category/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,guid,name,description,image,created_by,created_at,updated_by,updated_at,deleted_by,deleted_at,delete_status")] RBL_Category rBL_Category)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rBL_Category).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(rBL_Category);
        }

        // GET: RBL_Category/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RBL_Category rBL_Category = db.RBL_Category.Find(id);
            if (rBL_Category == null)
            {
                return HttpNotFound();
            }
            return View(rBL_Category);
        }

        // POST: RBL_Category/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            RBL_Category rBL_Category = db.RBL_Category.Find(id);
            db.RBL_Category.Remove(rBL_Category);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
