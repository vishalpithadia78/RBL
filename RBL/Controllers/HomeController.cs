﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RBL.Models;
using PagedList;
using System.Net;
using System.Web.Security;
using RBL.Enums;

namespace RBL.Controllers
{
    [AllowAnonymous]
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            var model = new HomeViewModel();
            model.PrentCategory = getAllCategory(true).Select(x => new SelectListItem { Text = x.name, Value = Convert.ToString(x.id)}).ToList();
            model.SubCategory = getAllCategory(false).Select(x => new SubCategoryView { Id = x.id, ParentId = x.parent_id,Name = x.name, Description= x.description,Image= x.image }).ToList();
            return View("Index", "_FrontendLayout",model);
        }

        [HttpPost]
        public ActionResult HitCount(int id)
        {

            using (var db = new  mobisnuu_rblEntities())
            {
                var dbEntry = db.RBL_MediaMaster.Where(x => x.id == id).FirstOrDefault();
                if (dbEntry != null)
                {
                    var count = dbEntry.download_count ?? 0;
                    dbEntry.download_count = ++count;
                    db.SaveChanges();
                }
            }
            return new HttpStatusCodeResult(200);
        }

        [Authorize]
        public void Logout()
        {
            
            using (var db = new mobisnuu_rblEntities())
            {
                var id = (long)Session["uid"];
                var dbUser = db.RBL_UserMaster.Where(x => x.id == id).FirstOrDefault();
                if (dbUser != null)
                {
                    if (!string.IsNullOrEmpty(dbUser.token_key) && Session.SessionID == dbUser.token_key)
                    {                     
                        dbUser.token_key = string.Empty;
                        db.SaveChanges();
                    }
                }
            }
            Session.Remove("uid");
            Session.Remove("uname");
            Session.Remove("permission");
            Session.Clear();
            Session.Abandon();
            Session.RemoveAll();
            if (Request.Cookies["ASP.NET_SessionId"] != null)
            {
                Response.Cookies["ASP.NET_SessionId"].Value = string.Empty;
                Response.Cookies["ASP.NET_SessionId"].Expires = DateTime.Now.AddMonths(-20);
            }
            if (Request.Cookies["DotNetOpenAuth.WebServerClient.XSRF-Session"] != null)
            {
                Response.Cookies["DotNetOpenAuth.WebServerClient.XSRF-Session"].Value = string.Empty;
                Response.Cookies["DotNetOpenAuth.WebServerClient.XSRF-Session"].Expires = DateTime.Now.AddMonths(-20);
            }
            FormsAuthentication.SignOut();
            Response.Redirect("~/Account/Login"); 
        }

        [Authorize]
        public ActionResult Dashboard()
        {
            var prms = (long?[])Session["permission"];
            if (RBL.Helpers.Utility.turnOffAuth() || (prms == null))
            {
                return RedirectToAction("Login", "Account");
            }
            if (Helpers.Utility.turnOffAuth() || ( !prms.Contains((int)Permission.CategoryView) && !prms.Contains((int)Permission.UserView) && !prms.Contains((int)Permission.FileView)))
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
            var model = new Dashboard();
            using (var db = new mobisnuu_rblEntities())
            {
                var allfile = db.RBL_MediaMaster.ToList();
                model.Total = allfile.Where(x => x.filetype_parent_id != null).ToList().Count();
                model.video = allfile.Where(x => x != null && x.filetype_parent_id == 2).Select(x => x.download_count).Sum();
                model.Image = allfile.Where(x => x != null && x.filetype_parent_id == 1).Select(x => x.download_count).Sum();
                model.pdf = allfile.Where(x => x != null && x.filetype_parent_id == 3).Select(x => x.download_count).Sum();
            }
            return View(model);
        }

        [NonAction]
        public IEnumerable<RBL_Category> getAllCategory(bool parent)
        {
            try
            {
                using (var db = new mobisnuu_rblEntities())
                {
                    List<RBL_Category> categories;
                    if (parent)
                    {
                        categories = db.RBL_Category.Where(x => x.delete_status == false && x.is_active == true && x.parent_id == 0).ToList();
                    }
                    else
                    {
                        categories = db.RBL_Category.Where(x => x.delete_status == false && x.is_active == true && x.parent_id != 0).ToList();
                    }
                    return categories;
                }
            }
            catch (Exception ex)
            {
                return new List<RBL_Category>();
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpGet]
        public ActionResult Detail(long? id)
        {
            
            if (id != null)
            {
                RBL_MediaMaster dbObj;
                using (var db = new mobisnuu_rblEntities())
                {
                   dbObj = db.RBL_MediaMaster.Where(x => x.id == id).FirstOrDefault();
                }
                if (dbObj != null)
                {
                    return View(getDetailViewModel(id));
                }
                else
                {
                    return new HttpStatusCodeResult(HttpStatusCode.NotFound);
                }

            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [NonAction]
        public  DetailViewModel getDetailViewModel(long? id)
        {
            var viewmodel = new DetailViewModel();
            //some Common fields 
            viewmodel.CategoryList = getAllCategory(true).Select(x => new SelectListItem { Text = x.name, Value = Convert.ToString(x.id) }).ToList();
            viewmodel.SubCategoryList = getAllCategory(false).Select(x => new SubCategory { Id = x.parent_id, Value = x.id, Text = x.name }).ToList();

            using (var db = new mobisnuu_rblEntities())
            {
                var dbObj = db.RBL_MediaMaster.Where(x => x.id == id).FirstOrDefault();
                viewmodel.Title = dbObj.title;
                viewmodel.Description = dbObj.description;
                viewmodel.Date = dbObj.created_at;
                var authorDbObj = db.RBL_UserMaster.Find(dbObj.user_master_id);
                if (authorDbObj != null)
                {
                    viewmodel.Author = authorDbObj.name;
                    viewmodel.AuthotEmail = authorDbObj.email;
                    viewmodel.Mobile = String.IsNullOrEmpty(authorDbObj.phone_no1) ? authorDbObj.phone_no2:authorDbObj.phone_no1;
                }
                else
                {
                    viewmodel.Author = "admin";
                    viewmodel.AuthotEmail = "admin@rbl.co";
                    viewmodel.Mobile = "9856325680";
                }
                viewmodel.Keywords = getAllKeyword(id);
                viewmodel.Location = getAllLocation(id);
                viewmodel.Language = getAllLanguage(id);
                viewmodel.allFile = getRelatedFiles(id);
                viewmodel.allFileTypes = viewmodel.allFile.Select(x => (int)x.filetype_parent_id).ToList();
            }
            return viewmodel;

        }

        private List<string> getAllLanguage(long? id)
        {
            using (var db = new mobisnuu_rblEntities())
            {
                return db.view_language_relationship.Where(x => x.media_master_id == id).Select(x => x.name).ToList();
            }
        }

        private List<RBL_MediaMaster> getRelatedFiles(long? id)
        {
            List<dynamic> listOfFiles;
            using (var db = new mobisnuu_rblEntities())
            {
                var list = db.RBL_MediaMaster.Where(x => x.id == id || x.parent_id == id).ToList();
                if (list != null)
                {
                    var firstOfList = list.ElementAt(0);
                    if (firstOfList.is_project && firstOfList.parent_id == 0)
                    {
                        listOfFiles = new List<dynamic>();
                        list.RemoveAt(0);
                        return list;
                    }
                    else if (!list.ElementAt(0).is_project)
                    {
                        return list;
                    }
                    else
                    {
                        System.Diagnostics.Debug.WriteLine("Couldnt not find first object");
                        throw new Exception("Couldnt Find first Object");
                    }
                }
                else
                {
                    throw new Exception("Couldnt find any object from database");
                }
            }
        }

        private List<string> getAllLocation(long? id)
        {
            using (var db = new mobisnuu_rblEntities())
            {
                return db.view_location_relationship.Where(x => x.media_master_id == id).Select(x => x.name).ToList();
            }
        }

        private List<string> getAllKeyword(long? id)
        {
            using (var db = new mobisnuu_rblEntities())
            {
                return db.view_keyword_relationship.Where(x => x.media_master_id == id).Select(x => x.name).ToList();
            }
        }

        [HttpGet]
        public ActionResult Search(SearchViewModel model)
        {
            var db = new mobisnuu_rblEntities();
            model.Page = model.Page ?? 1;
            model.PageSize = model.PageSize ?? 25;
            var viewModel = getSearchViewModel(model);
            //if (!String.IsNullOrEmpty(model.SearchText)) // if something is searched
            //{
                var sm = new SearchModel(model);
                
                ViewBag.list = sm.getSearchResults().ToPagedList((int)model.Page,(int)model.PageSize);

            //}
            //else if (model.ParentId != null && model.Id != null) // if only sub category is chosen
            //{
            //    var listOf = db.RBL_MediaMasterCategory.Where( x => x.category_id == model.ParentId && x.category_child_id == model.Id).Select(x=>x.media_master_id).ToList();
            //    ViewBag.list = db.RBL_MediaMaster.Where(x => x.parent_id == 0 && listOf.Contains(x.id)).ToList();
            //}

            if (model == null)
            {
                ViewBag.list = new List<RBL_MediaMaster>();
            }
            return View("Search", "_FrontendLayout", viewModel);
        }

        [NonAction]
        private SearchViewModel getSearchViewModel( SearchViewModel model)
        {
            var viewModel = new SearchViewModel();
            viewModel.FileTypeList = new List<SelectListItem> { /*new SelectListItem { Text = "All", Value = "0" },*/ new SelectListItem { Text = "Image", Value = "1" }, new SelectListItem { Text = "Video", Value = "2" }, new SelectListItem { Text = "PDF", Value = "3" } };
            viewModel.Language = (List<string>)ListingModelHelper.getAllLanguage();
            viewModel.Locations = (List<string>)ListingModelHelper.getAllLocation();
            viewModel.Locations = (List<string>)ListingModelHelper.getAllLocation();
            viewModel.SelectedFileTypes = model.SelectedFileTypes;
            viewModel.SelectedLanguage = model.SelectedLanguage;
            viewModel.SelectedLocation = model.SelectedLocation;
            viewModel.SelectedYear = model.SelectedYear;
            viewModel.Id = model.Id;
            viewModel.ParentId = model.ParentId;
            viewModel.Page = model.Page;
            viewModel.PageSize = model.PageSize;
            viewModel.pdfType = model.pdfType;
            viewModel.Year = ListingModelHelper.getAllYears().Select(x => x.ToString()).ToList();
            viewModel.CategoryList = new SelectList(getAllCategory(true), "id", "name");
            viewModel.SubCategoryList = getAllCategory(false).Select(x => new SubCategory { Id = x.parent_id, Value = x.id, Text = x.name }).ToList();
            viewModel.PdfTypeList = new List<SelectListItem> { new SelectListItem { Text = "Leaflets", Value = "4" }, new SelectListItem { Text = "Brochures", Value = "6" }, new SelectListItem { Text = "Forms", Value = "5" } };
            return viewModel;
        }
    }
}