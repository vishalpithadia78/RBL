﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="RBL.Content._default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <h1>
    This is restricted area. Kindly click <a href="<%=ResolveUrl("~/") %>">here</a> to go to homepage
    </h1>
</body>
</html>
