use mobisnuu_rbl
-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
Drop proc if exists usp_RBL_AddProject
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE usp_RBL_AddProject
	-- Add the parameters for the stored procedure here
	@fileList [FileList] readonly,
	@keywords varchar(max),
	@locations varchar(max),
	@languages varchar(max),
	@categoryid bigint ,
	@category_childid bigint,
	@title varchar(200),
	@description varchar(200),
	@is_approved  bit = 1,
	@is_public bit = 1,
	@is_project bit = 1,
	@Userid bigint 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- Insert statements for procedure here

DECLARE @OutputTbl TABLE (ID INT)
DECLARE @LastMasterID INT 
DECLARE @TaxonomyTable TABLE(ITEM VARCHAR(60),[type] varchar(10),ID BIGINT)

begin tran

begin try

-- start of master adding 
INSERT INTO [dbo].[RBL_MediaMasterV2]
           ([guid]
           ,[title]
           ,[description]
           ,[category_id]
           ,[category_child_id]
           ,[is_public]
           ,[is_project]
           ,[is_approved]
           ,[created_by]
           ,[updated_by])
	 OUTPUT INSERTED.ID INTO @OutputTbl(ID)
     VALUES
           (newid()
           ,@title
           ,@description
           ,@categoryid
           ,@category_childid
           ,@is_public
           ,@is_project
           ,@is_approved
           ,@Userid
           ,@Userid)

select @LastMasterID = ID  from @OutputTbl

--- end of master adding

-- taxonomy adding process started	
/* preparing keywords*/
INSERT INTO @TaxonomyTable(ITEM,[type])
SELECT LOWER(rtrim(ltrim(value))) as ITEM, 'Keyword' as [type]
FROM STRING_SPLIT(@keywords, ',')  

/* preparing location*/
INSERT INTO @TaxonomyTable(ITEM,[type]) 
SELECT LOWER(rtrim(ltrim(value))), 'Location' as [type]
FROM STRING_SPLIT(@locations, ',')  

/* preparing languages*/
INSERT INTO @TaxonomyTable(ITEM,[type])
SELECT LOWER(rtrim(ltrim(value))), 'Language' as [type]
FROM STRING_SPLIT(@languages, ',')  

declare @taxonomyItem varchar(200),@taxonomyType varchar(10),@taxnonomyId bigint

declare taxonomy_cursor cursor for select * from @TaxonomyTable FOR UPDATE OF ID

declare @lastInsertedId bigint = 0


OPEN taxonomy_cursor 
FETCH NEXT FROM taxonomy_cursor   
	INTO @taxonomyItem, @taxonomyType ,@taxnonomyId
WHILE @@FETCH_STATUS = 0  
BEGIN  
	select top 1 @lastInsertedId = tm.Id from RBL_TaxonomyMasterV2  tm where  tm.[name] = @taxonomyItem and tm.[type] = @taxonomyType 
	if not( @lastInsertedId > 0)
	begin
		INSERT INTO [dbo].[RBL_TaxonomyMasterV2]
				   ([name]
				   ,[type]
				   ,[created_by]
				   ,[updated_by])
				OUTPUT INSERTED.ID INTO @OutputTbl(ID)
			 VALUES
				   (@taxonomyItem
				   ,@taxonomyType
				   , @Userid
				   ,@Userid)
		select @lastInsertedId = ID  from @OutputTbl
	end
	UPDATE @TaxonomyTable SET ID = @lastInsertedId WHERE CURRENT OF taxonomy_cursor
--	select @taxonomyItem,@taxonomyType,@lastInsertedId
	set @lastInsertedId = 0
	FETCH NEXT FROM taxonomy_cursor   
	INTO @taxonomyItem, @taxonomyType ,@taxnonomyId
END   
CLOSE taxonomy_cursor;  
DEALLOCATE taxonomy_cursor;  

--select * from @TaxonomyTable


INSERT INTO [dbo].[RBL_TaxonomyMediaMasterMappingV2]
           ([media_master_id]
           ,[taxonomy_master_id]
           ,[type]
           ,[created_by]
           ,[updated_by])
       SELECT  
		   @LastMasterID as media_master_id,
		   tt.ID as taxonomy_master_id,
		   tt.[type] as [type],
		   @Userid as created_by,
		   @Userid as updated_by
	   FROM @TaxonomyTable tt


--- end of taxonomy adding

-- start of inserting images to db

INSERT INTO [dbo].[RBL_MediaMasterRAWFilesV2]
           ([filetype_child_id]
           ,[filetype_parent_id]
           ,[originalfileName]
           ,[media_master_id]
           ,[thumbnail_bytes]
           ,[file_bytes]
           ,[size]
           ,[extension]
           ,[is_approved]
           ,[is_active]
           ,[created_by]
           ,[updated_by])
  
  select 
  fl.[filetype_child_id] as [filetype_child_id],
  fl.[filetype_parent_id] as [filetype_parent_id],
  fl.[originalfileName] as [originalfileName] ,
  @LastMasterID as [media_master_id],
  fl.thumb as [thumbnail_bytes],
  fl.[file_bytes] as [file_bytes],
  datalength(fl.[file_bytes]) as [size],
  fl.[extension] as [extension],
  fl.[is_approved] as [is_approved],
  fl.[is_active] as [is_active],
  @Userid as [created_by],
  @Userid  as [updated_by]
  from @filelist fl

-- end of inserting images to db
end try
begin catch
rollback tran
select 0 as inserted
end catch
select 1 as inserted
commit tran
END
GO
