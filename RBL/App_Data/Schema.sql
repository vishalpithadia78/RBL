USE [mobisnuu_rbl]
GO
/****** Object:  Table [dbo].[RBL_Category]    Script Date: 18-05-2017 17:37:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RBL_Category](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[guid] [varchar](80) NOT NULL CONSTRAINT [DF_RBL_Category_guid]  DEFAULT (newid()),
	[name] [varchar](60) NULL,
	[description] [varchar](150) NULL,
	[image] [varchar](80) NULL,
	[parent_id] [bigint] NULL,
	[is_active] [bit] NOT NULL CONSTRAINT [DF_RBL_Category_is_active]  DEFAULT ((1)),
	[created_by] [bigint] NULL,
	[created_at] [datetime] NULL,
	[updated_by] [bigint] NULL,
	[updated_at] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_at] [datetime] NULL,
	[delete_status] [bit] NOT NULL CONSTRAINT [DF_RBL_Category_delete_status]  DEFAULT ((0)),
 CONSTRAINT [PK_RBL_Category] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RBL_FileType]    Script Date: 18-05-2017 17:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RBL_FileType](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[guid] [varchar](80) NOT NULL,
	[filetype_name] [varchar](70) NULL,
	[parent_id] [bigint] NULL,
	[created_by] [bigint] NULL,
	[created_at] [datetime] NULL,
	[updated_by] [bigint] NULL,
	[updated_at] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_at] [datetime] NULL,
	[delete_status] [bit] NOT NULL,
 CONSTRAINT [PK_RBL_FileType] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RBL_Keyword]    Script Date: 18-05-2017 17:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RBL_Keyword](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[guid] [varchar](80) NOT NULL CONSTRAINT [DF_RBL_Keyword_guid]  DEFAULT (newid()),
	[name] [varchar](60) NULL,
	[created_by] [bigint] NULL,
	[created_at] [datetime] NULL,
	[updated_by] [bigint] NULL,
	[updated_at] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_at] [datetime] NULL,
	[delete_status] [bit] NOT NULL CONSTRAINT [DF_RBL_Keyword_delete_status]  DEFAULT ((0)),
 CONSTRAINT [PK_RBL_Keyword] PRIMARY KEY CLUSTERED 
(
	[guid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RBL_Language]    Script Date: 18-05-2017 17:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RBL_Language](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[guid] [varchar](80) NOT NULL CONSTRAINT [DF_RBL_Language_guid]  DEFAULT (newid()),
	[name] [varchar](60) NULL,
	[created_by] [bigint] NULL,
	[created_at] [datetime] NULL,
	[updated_by] [bigint] NULL,
	[updated_at] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_at] [datetime] NULL,
	[delete_status] [bit] NOT NULL CONSTRAINT [DF_RBL_Language_delete_status]  DEFAULT ((0)),
 CONSTRAINT [PK_RBL_Language] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RBL_Location]    Script Date: 18-05-2017 17:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RBL_Location](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[guid] [varchar](80) NOT NULL CONSTRAINT [DF_RBL_Location_guid]  DEFAULT (newid()),
	[name] [varchar](60) NULL,
	[created_by] [bigint] NULL,
	[created_at] [datetime] NULL,
	[updated_by] [bigint] NULL,
	[updated_at] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_at] [datetime] NULL,
	[delete_status] [bit] NOT NULL CONSTRAINT [DF_RBL_Location_delete_status]  DEFAULT ((0)),
 CONSTRAINT [PK_RBL_Location] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RBL_MediaMaster]    Script Date: 18-05-2017 17:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RBL_MediaMaster](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[user_master_id] [bigint] NOT NULL,
	[guid] [varchar](80) NOT NULL CONSTRAINT [DF_RBL_MediaMaster_guid]  DEFAULT (newid()),
	[title] [varchar](100) NULL,
	[description] [varchar](150) NULL,
	[thumbnail] [varchar](400) NULL,
	[filetype_child_id] [bigint] NULL,
	[filetype_parent_id] [bigint] NULL,
	[filename] [varchar](400) NULL,
	[size] [varchar](150) NULL,
	[is_public] [bit] NOT NULL CONSTRAINT [DF_RBL_MediaMaster_is_public]  DEFAULT ((1)),
	[is_project] [bit] NOT NULL CONSTRAINT [DF_RBL_MediaMaster_is_project]  DEFAULT ((0)),
	[extension] [varchar](10) NULL,
	[parent_id] [bigint] NULL,
	[download_count] [bigint] NULL,
	[is_approved] [bit] NOT NULL CONSTRAINT [DF_RBL_MediaMaster_is_approved]  DEFAULT ((0)),
	[is_active] [bit] NOT NULL CONSTRAINT [DF_RBL_MediaMaster_is_active]  DEFAULT ((1)),
	[created_by] [bigint] NULL,
	[created_at] [datetime] NULL,
	[updated_by] [bigint] NULL,
	[updated_at] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_at] [datetime] NULL,
	[delete_status] [bit] NOT NULL CONSTRAINT [DF_RBL_MediaMaster_delete_status]  DEFAULT ((0)),
 CONSTRAINT [PK_RBL_MediaMaster] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RBL_MediaMasterCategory]    Script Date: 18-05-2017 17:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RBL_MediaMasterCategory](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[guid] [varchar](80) NOT NULL CONSTRAINT [DF_RBL_MediaCategory_guid]  DEFAULT (newid()),
	[media_master_id] [bigint] NULL,
	[category_child_id] [bigint] NULL,
	[category_id] [bigint] NULL,
	[created_by] [bigint] NULL,
	[created_at] [datetime] NULL,
	[updated_by] [bigint] NULL,
	[updated_at] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_at] [datetime] NULL,
	[delete_status] [bit] NOT NULL CONSTRAINT [DF_RBL_MediaCategory_delete_status]  DEFAULT ((0)),
 CONSTRAINT [PK_RBL_MediaCategory] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RBL_MediaMasterKeyword]    Script Date: 18-05-2017 17:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RBL_MediaMasterKeyword](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[guid] [varchar](80) NOT NULL CONSTRAINT [DF_RBL_MediaMasterKeyword_guid]  DEFAULT (newid()),
	[media_master_id] [bigint] NULL,
	[keyword_id] [bigint] NULL,
	[created_by] [bigint] NULL,
	[created_at] [datetime] NULL,
	[updated_by] [bigint] NULL,
	[updated_at] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_at] [datetime] NULL,
	[delete_status] [bit] NOT NULL CONSTRAINT [DF_RBL_MediaMasterKeyword_delete_status]  DEFAULT ((0)),
 CONSTRAINT [PK_RBL_MediaMasterKeyword] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RBL_MediaMasterLanguage]    Script Date: 18-05-2017 17:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RBL_MediaMasterLanguage](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[guid] [varchar](80) NOT NULL CONSTRAINT [DF_RBL_MediaMasterLanguage_guid]  DEFAULT (newid()),
	[media_master_id] [bigint] NULL,
	[language_id] [bigint] NULL,
	[created_by] [bigint] NULL,
	[created_at] [datetime] NULL,
	[updated_by] [bigint] NULL,
	[updated_at] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_at] [datetime] NULL,
	[delete_status] [bit] NOT NULL CONSTRAINT [DF_RBL_MediaMasterLanguage_delete_status]  DEFAULT ((0)),
 CONSTRAINT [PK_RBL_MediaMasterLanguage] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RBL_MediaMasterLocation]    Script Date: 18-05-2017 17:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RBL_MediaMasterLocation](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[guid] [varchar](80) NOT NULL CONSTRAINT [DF_RBL_MediaMasterLocation_guid]  DEFAULT (newid()),
	[media_master_id] [bigint] NULL,
	[location_id] [bigint] NULL,
	[created_by] [bigint] NULL,
	[created_at] [datetime] NULL,
	[updated_by] [bigint] NULL,
	[updated_at] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_at] [datetime] NULL,
	[delete_status] [bit] NOT NULL CONSTRAINT [DF_RBL_MediaMasterLocation_delete_status]  DEFAULT ((0)),
 CONSTRAINT [PK_RBL_MediaMasterLocation] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RBL_Menu]    Script Date: 18-05-2017 17:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RBL_Menu](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[guid] [varchar](80) NOT NULL CONSTRAINT [DF_RBL_Menu_guid]  DEFAULT (newid()),
	[parent_id] [bigint] NULL,
	[parent_name] [varchar](50) NULL,
	[action_name] [varchar](50) NULL,
	[icon] [varchar](100) NULL,
	[created_by] [bigint] NULL,
	[created_at] [datetime] NULL,
	[updated_by] [bigint] NULL,
	[updated_at] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_at] [datetime] NULL,
	[delete_status] [bit] NOT NULL CONSTRAINT [DF_RBL_Menu_delete_status]  DEFAULT ((0)),
 CONSTRAINT [PK_RBL_Menu] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RBL_RoleName]    Script Date: 18-05-2017 17:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RBL_RoleName](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[guid] [varchar](80) NOT NULL CONSTRAINT [DF_RBL_RoleName_guid]  DEFAULT (newid()),
	[role_name] [varchar](50) NULL,
	[is_active] [bit] NULL CONSTRAINT [DF_RBL_RoleName_is_active]  DEFAULT ((1)),
	[created_by] [bigint] NULL,
	[created_at] [datetime] NULL,
	[updated_by] [bigint] NULL,
	[updated_at] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_at] [datetime] NULL,
	[delete_status] [bit] NOT NULL CONSTRAINT [DF_RBL_RoleName_delete_status]  DEFAULT ((0)),
 CONSTRAINT [PK_RBL_RoleName] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RBL_RolePermission]    Script Date: 18-05-2017 17:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RBL_RolePermission](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[guid] [varchar](80) NOT NULL CONSTRAINT [DF_RBL_RolePermission_guid]  DEFAULT (newid()),
	[role_id] [bigint] NULL,
	[menu_id] [bigint] NULL,
	[created_by] [bigint] NULL,
	[created_at] [datetime] NULL,
	[updated_by] [bigint] NULL,
	[updated_at] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_at] [datetime] NULL,
	[delete_status] [bit] NOT NULL CONSTRAINT [DF_RBL_RolePermission_delete_status]  DEFAULT ((0)),
 CONSTRAINT [PK_RBL_RolePermission] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RBL_UserMaster]    Script Date: 18-05-2017 17:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RBL_UserMaster](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[guid] [varchar](80) NOT NULL CONSTRAINT [DF_Table_UserMaster_guid]  DEFAULT (newid()),
	[name] [varchar](50) NULL,
	[email] [varchar](50) NULL,
	[emp_code] [varchar](50) NULL,
	[phone_no1] [varchar](15) NULL,
	[phone_no2] [varchar](15) NULL,
	[password] [varchar](200) NULL,
	[profile_img] [varchar](100) NULL,
	[role_id] [bigint] NULL,
	[is_active] [bit] NOT NULL CONSTRAINT [DF_RBL_UserMaster_is_active]  DEFAULT ((1)),
	[verification_status] [bit] NOT NULL CONSTRAINT [DF_RBL_UserMaster_verification_status]  DEFAULT ((0)),
	[verification_hash] [varchar](150) NULL CONSTRAINT [DF_RBL_UserMaster_verification_hash]  DEFAULT (newid()),
	[last_login_at] [datetime] NULL,
	[token_key] [varchar](150) NULL,
	[created_by] [bigint] NULL,
	[created_at] [datetime] NULL,
	[updated_by] [bigint] NULL,
	[updated_at] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[deleted_at] [datetime] NULL,
	[delete_status] [bit] NOT NULL CONSTRAINT [DF_Table_User_delete_status]  DEFAULT ((0)),
 CONSTRAINT [PK_Table_User] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[view_complete_category]    Script Date: 18-05-2017 17:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_complete_category]
AS
SELECT        dbo.RBL_Category.id AS parent_id, RBL_Category_1.id AS child_id, RBL_Category_1.name AS child_name, dbo.RBL_Category.name AS parent_name, RBL_Category_1.description AS child_description, 
                         RBL_Category_1.image AS child_image
FROM            dbo.RBL_Category LEFT OUTER JOIN
                         dbo.RBL_Category AS RBL_Category_1 ON dbo.RBL_Category.id = RBL_Category_1.parent_id
WHERE        (dbo.RBL_Category.is_active = 1) AND (dbo.RBL_Category.parent_id = 0)


GO
/****** Object:  View [dbo].[view_category_relationship]    Script Date: 18-05-2017 17:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_category_relationship]
AS
SELECT        dbo.view_complete_category.parent_name, dbo.view_complete_category.child_name, dbo.RBL_MediaMasterCategory.media_master_id, dbo.view_complete_category.parent_id AS category_id, 
                         dbo.view_complete_category.child_id
FROM            dbo.view_complete_category INNER JOIN
                         dbo.RBL_MediaMasterCategory ON dbo.view_complete_category.child_id = dbo.RBL_MediaMasterCategory.category_child_id AND 
                         dbo.view_complete_category.parent_id = dbo.RBL_MediaMasterCategory.category_id


GO
/****** Object:  View [dbo].[view_keyword_relationship]    Script Date: 18-05-2017 17:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_keyword_relationship]
AS
SELECT        dbo.RBL_Keyword.name, dbo.RBL_Keyword.id, dbo.RBL_MediaMasterKeyword.media_master_id
FROM            dbo.RBL_Keyword INNER JOIN
                         dbo.RBL_MediaMasterKeyword ON dbo.RBL_Keyword.id = dbo.RBL_MediaMasterKeyword.keyword_id


GO
/****** Object:  View [dbo].[view_language_relationship]    Script Date: 18-05-2017 17:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_language_relationship]
AS
SELECT        dbo.RBL_Language.name, dbo.RBL_MediaMasterLanguage.media_master_id, dbo.RBL_Language.id
FROM            dbo.RBL_Language INNER JOIN
                         dbo.RBL_MediaMasterLanguage ON dbo.RBL_Language.id = dbo.RBL_MediaMasterLanguage.language_id


GO
/****** Object:  View [dbo].[view_location_relationship]    Script Date: 18-05-2017 17:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_location_relationship]
AS
SELECT        dbo.RBL_Location.id, dbo.RBL_Location.name, dbo.RBL_MediaMasterLocation.media_master_id
FROM            dbo.RBL_Location INNER JOIN
                         dbo.RBL_MediaMasterLocation ON dbo.RBL_Location.id = dbo.RBL_MediaMasterLocation.location_id


GO
/****** Object:  View [dbo].[view_complete_master]    Script Date: 18-05-2017 17:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_complete_master]
AS
SELECT        dbo.RBL_MediaMaster.title, dbo.RBL_MediaMaster.id, dbo.view_category_relationship.child_name, dbo.view_category_relationship.parent_name, dbo.view_keyword_relationship.name AS keword, 
                         dbo.view_language_relationship.name AS language, dbo.view_location_relationship.name AS location, dbo.view_category_relationship.category_id AS parent_cat_id, 
                         dbo.view_category_relationship.child_id AS child_cat_id, dbo.RBL_MediaMaster.filetype_parent_id, dbo.RBL_MediaMaster.filetype_child_id, dbo.RBL_MediaMaster.created_at AS date, 
                         dbo.RBL_MediaMaster.description, dbo.RBL_MediaMaster.parent_id, dbo.RBL_MediaMaster.is_active
FROM            dbo.RBL_MediaMaster LEFT OUTER JOIN
                         dbo.view_category_relationship ON dbo.RBL_MediaMaster.id = dbo.view_category_relationship.media_master_id LEFT OUTER JOIN
                         dbo.view_keyword_relationship ON dbo.RBL_MediaMaster.id = dbo.view_keyword_relationship.media_master_id LEFT OUTER JOIN
                         dbo.view_language_relationship ON dbo.RBL_MediaMaster.id = dbo.view_language_relationship.media_master_id LEFT OUTER JOIN
                         dbo.view_location_relationship ON dbo.RBL_MediaMaster.id = dbo.view_location_relationship.media_master_id

GO
ALTER TABLE [dbo].[RBL_FileType] ADD  CONSTRAINT [DF_RBL_FileType_guid]  DEFAULT (newid()) FOR [guid]
GO
ALTER TABLE [dbo].[RBL_FileType] ADD  CONSTRAINT [DF_RBL_FileType_delete_status]  DEFAULT ((0)) FOR [delete_status]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "RBL_MediaMaster (dbo)"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 220
            End
            DisplayFlags = 280
            TopColumn = 14
         End
         Begin Table = "view_category_relationship (dbo)"
            Begin Extent = 
               Top = 6
               Left = 258
               Bottom = 136
               Right = 436
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "view_keyword_relationship (dbo)"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 251
               Right = 216
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "view_language_relationship (dbo)"
            Begin Extent = 
               Top = 138
               Left = 254
               Bottom = 251
               Right = 432
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "view_location_relationship (dbo)"
            Begin Extent = 
               Top = 252
               Left = 38
               Bottom = 365
               Right = 216
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder =' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'view_complete_master'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N' 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'view_complete_master'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'view_complete_master'
GO
