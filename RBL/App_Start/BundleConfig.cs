﻿using System.Web;
using System.Web.Optimization;

namespace RBL
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/Helper.js",
                      "~/Scripts/respond.js",
                      "~/Scripts/bootstrap-filestyle.js",
                      "~/Scripts/bootstrap-tagsinput.min.js",
                      "~/Scripts/bootstrap3-typeahead.js"));

            bundles.Add(new StyleBundle("~/Styles/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/sb-admin.css",
                      "~/Content/bootstrap-tagsinput.css",
                      "~/Content/style.css",
                      "~/Content/media.css",
                       "~/Content/css/font-awesome.css"));

            bundles.Add(new ScriptBundle("~/Scripts/frontend/js").Include(
                    "~/Content/frontend/js/bootstrap.js",
                    "~/Content/frontend/js/jquery.easing.js",
                    "~/Content/frontend/js/jquery.flexslider.js",
                    "~/Content/frontend/js/demo.js"));


            bundles.Add(new StyleBundle("~/Styles/frontend").Include(
                     "~/Content/frontend/bootstrap.css",
                     "~/Content/frontend/demo.css",
                     "~/Content/frontend/flexslider.css",
                      "~/Content/frontend/style.css",
                     "~/Content/frontend/media.css",
                      "~/Content/frontend/fontello/css/fontello.css"));
        }
    }
}
